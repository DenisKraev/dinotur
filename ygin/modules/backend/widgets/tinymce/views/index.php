<?php

/**
 * @var $form CActiveForm
 * @var $this TinymceWidget
 * @var $tiny ETinyMceYgin
 */

$contentCss = '';
$theme = Yii::app()->getFrontendTheme();
if ($theme != null && file_exists($theme->basePath.'/css/content.css')) {
  $contentCss = $theme->baseUrl.'/css/content.css';
}

$tiny = $this->widget('ygin.ext.tinymce.ETinyMceYgin', CMap::mergeArray(array(
  'model' => $model,
  'attribute' => $attributeName,
//  'addPlugin' => array('ygin_clean'),
  'addAdvancedStyles' => array(
    'Таблица в тексте без границ' => 'cTable',
    'Таблица в тексте с границами' => 'cTableB',
    'Открытие фото в попапе' => 'js-popup-img',
    'Заголовок'=>'post-title',
    'Подзаголовок'=>'post-subtitle',
    'Текст в контактах'=>'post-p',
    'Ссылки'=>'post-a',
    'Ссылки для аякс'=>'post-a-this-page',
    'Блок для картинки в постах'=>'post-image',
    'Картинка в постах'=>'post-image-img',
    'Блок с текстом в постах'=>'post-text',
    'Для вызова карты в попапе'=>'js-map-popup',
  ),
//  'addButton' => array(
//    array('row'=>3, 'button'=>'ygin_clean', 'before'=>'code'),
//  ),
  'addOption' => array(
    'skin' => 'bootstrap',
    'ygin_advlink_show_block_menu' => true,
    'ygin_advlink_show_block_files' => true,
    //'setup' => 'js:function(ed) {ed.onPostProcess.add(function(ed, o) {o.content = cleanCode(o.content, true, false, true, true, true, true, true, true, true);});}',
  ),
  'contentCss' => $contentCss,
), $this->options));

$assetsDir = $tiny->assetsPath;
Yii::app()->clientScript->registerScriptFile($assetsDir.'/jquery/functions.js');

echo $form->error($model, $attributeName);