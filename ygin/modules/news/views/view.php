<?php $this->pageTitle = $model->title.' | '.Yii::app()->name;  ?>

<div class="container inner-content">
    <div class="box-news-single">
        <h1><?php echo $model->title; ?></h1>
        <div class="date">
            <?php echo Yii::app()->dateFormatter->formatDateTime($model->date, 'long', null); ?>
        </div>
        <div class="text"><?php echo $model->content; ?></div>
        <div class="archive">«&nbsp;<?php echo CHtml::link(Yii::t('main-ui', 'К списку новостей'), array('index'))?></div>
    </div>
</div>

