<?php
/**
 * @var $menu Menu
 * @var $this StaticController
 */
if ($menu->go_to_type == Menu::SHOW_INCLUDED_ITEMS_BEFORE_CONTENT) {
  $this->renderPartial('/treeMenu', array('menu' => $menu));
  echo '<br>';
}
$c = Yii::app()->language == 'ru' ? 'content' : 'content_'.Yii::app()->language;
echo $menu->$c;
if ($menu->go_to_type == Menu::SHOW_INCLUDED_ITEMS_AFTER_CONTENT) {
  echo '<br>';
  $this->renderPartial('/treeMenu', array('menu' => $menu));
}