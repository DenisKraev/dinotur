<?php $videoCover = $model->getParseYoutubeUrl($model->url) ?>

<div class="box-video ib">
    <div class="video-cover">
        <a href="<?php echo $videoCover['src']; ?>?wmode=transparent&rel=0&autoplay=1" class="js-popup-video iframe">
            <img alt="<?php echo AppHelper::cutQuotes($model->name); ?>" src="http://img.youtube.com/vi/<?php echo $videoCover['code']; ?>/0.jpg">
        </a>
    </div>
    <div class="name">
        <a href="<?php echo $videoCover['src']; ?>?wmode=transparent&rel=0&autoplay=1" class="js-popup-video iframe" title="<?php echo AppHelper::cutQuotes($model->name); ?>">
            <?php echo HText::smartCrop($model->name, 50); ?>
        </a>
    </div>
</div>