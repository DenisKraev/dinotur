<div class="container inner-content">

    <div class="box-videos">

        <div class="title-page title-photo-video">
            <a href="/<?php echo Yii::app()->language; ?>/photoalbum/" ><?php echo Yii::t('main-ui', 'Фотоальбомы'); ?></a>
            <span class="separator">/</span>
            <span class="active"><?php echo Yii::t('main-ui', 'Видеоальбом'); ?></span>
        </div>

        <div class="box-video-list">
            <?php foreach($model as $modelItem){
                $this->renderPartial('_view', array('model' => $modelItem));
            } ?>
        </div>

        <?php  $this->widget('LinkPagerWidget', array(
            'pages' => $pages,
        )); ?>

    </div>

</div>