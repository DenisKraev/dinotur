<div class="box-single-item ib">
    <?php if ($model->getImagePreview('_list')) { ?>
        <a href="<?php echo $model->getUrl(); ?>" class="box-img">
            <img src="<?php echo $model->getImagePreview('_list')->getUrlPath() ?>" alt="<?php echo AppHelper::cutQuotes($model->title); ?>" />
        </a>
    <?php } ?>
    <h3 class="name"><a href="<?php echo $model->getUrl(); ?>" title="<?php echo $model->title; ?>"><?php echo HText::smartCrop($model->title, 45); ?></a></h3>
    <div class="description"><?php echo AppHelper::Truncate($model->short, 220); ?></div>
    <div class="date"><?php echo Yii::app()->dateFormatter->formatDateTime($model->date, 'long', null); ?></div>
    <div class="more">
        <a class="brown-btn" href="<?php echo $model->getUrl(); ?>"><?php echo Yii::t('main-ui', 'Подробнее'); ?></a>
    </div>
</div>