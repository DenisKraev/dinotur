<?php ($model->page_title) ? $this->setPageTitle($model->page_title) : $this->setPageTitle($model->title.' | '.Yii::app()->name);?>
<?php $this->setKeywords($model->meta_keywords); ?>
<?php $this->setDescription($model->meta_description); ?>

<div class="container inner-content">
    <div class="box-news-single">
        <h1 class="title"><?php echo $model->title; ?></h1>
        <div class="date">
            <?php echo Yii::app()->dateFormatter->formatDateTime($model->date, 'long', null); ?>
        </div>
        <div class="text"><?php echo $model->content; ?></div>
        <div class="archive"><?php echo CHtml::link('«&nbsp;'.Yii::t('main-ui', 'К списку новостей'), array('index'), array('class' => 'brown-btn'))?></div>
    </div>
</div>

