<div id="footer">
    <div class="footer-content container">
        <div class="row">
            <div class="col-md-4">
                <div class="copy">
                    <?php $startCopyRight = '2014'; $startCopyRight = date('Y') == $startCopyRight ? $startCopyRight : $startCopyRight.' — '. date('Y');?>
                    <?php echo Yii::t('main-ui', 'Все права защищены &laquo;Дино Тур&raquo; &copy;').' '.$startCopyRight.' '.Yii::t('main-ui', 'г'); ?>
                </div>
            </div>
            <div class="col-md-4">
                <div class="footer-menu">
                    <?php
                        $this->widget('MenuWidget', array(
                            'rootItem' => Yii::app()->menu->all,
                            'typeMenu' => 'type_footer_menu',
//                            'addItems' => array(
//                                array(
//                                    'sequence' => 0,
//                                    'menu' => Yii::app()->AddMenuItems->getAddMenuInteresting($visibleChildLevels = false)
//                                ),
//                                array(
//                                    'sequence' => 1,
//                                    'menu' => Yii::app()->AddMenuItems->getAddMenuTour($visibleChildLevels = false)
//                                ),
//                                array(
//                                    'sequence' => 5,
//                                    'menu' => Yii::app()->AddMenuItems->getAddMenuSouvenir($visibleChildLevels = false)
//                                )
//                            ),
                            'htmlOptions' => array('class' => 'cf'),
                            'encodeLabel' => false,
                            'activateParents' => 'true'
                        ));
                    ?>
                </div>
            </div>
            <div class="col-md-4">
                <div class="soc-btns">
                    <span class="title"><?php echo Yii::t('main-ui', 'Мы в соцсетях'); ?>:</span>
                    <?php if (Yii::app()->Block->getBlockContent('vk_link') !== null) { ?>
                        <a href="<?php echo Yii::app()->Block->getBlockContent('vk_link'); ?>" class="vk-btn ib" target="_blank"></a>
                    <?php } ?>
                    <?php if (Yii::app()->Block->getBlockContent('fb_link') !== null) { ?>
                        <a href="<?php echo Yii::app()->Block->getBlockContent('fb_link'); ?>" class="fb-btn ib" target="_blank"></a>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="dev">
            <span><?php echo Yii::t('main-ui', 'Сделано в'); ?></span>
            <img src="/themes/application/images/app/artnet-logo.png" alt="Разработка и дизайн сайтов Artnet Studio" title="Разработка и дизайн сайтов Artnet Studio">
            <a href="http://artnetdesign.ru/" target="_blank">Artnet Studio</a>
        </div>
    </div>
</div>