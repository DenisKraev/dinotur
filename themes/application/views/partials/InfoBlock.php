<div class="box-info-block">
    <div class="dino"></div>
    <div class="container">
        <div class="cf">
            <?php if (Yii::app()->Block->getBlockContent('about_main_page') !== null) { ?>
                <div class="col-md-8">
                    <div class="box-about">
                        <h3 class="title-main"><a href="page/o_nas/"><?php echo Yii::t('main-ui', 'О нас'); ?></a></h3>
                        <div id="about-short-text" class="text box-popup-text">
                            <?php echo Yii::app()->Block->getBlockContent('about_main_page'); ?>
                        </div>
                        <div class="box-more"><a href="#about-short-text" class="more js-main-popup"><?php echo Yii::t('main-ui', 'Подробнее'); ?></a></div>
                    </div>
                </div>
            <?php } ?>
            <div class="col-md-4">
                <?php $this->widget('application.widgets.FlashNewsWidget', array('maxNews' => 2)); ?>
                <?php $this->widget('application.widgets.FlashRandomPhotoWidget', array(
                    'countPhoto' => 4,
                    'cropType' => 'center',
                    'width' => 127,
                    'height' => 97,
                    'watermark' =>  array(
                        'file' => Photoalbum::model()->getWatermark(Photoalbum::model()->idWatermarkThumb),
                        'align_y' => 'bottom',
                        'align_x' => 'right',
                    )
                )); ?>
            </div>
        </div>
    </div>
</div>