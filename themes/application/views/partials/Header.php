<div id="header">
    <div class="top-box">
        <div class="container">
            <div class="top-menu">
                <?php
                    $this->widget('MenuWidget', array(
                        'rootItem' => Yii::app()->menu->all,
                        'typeMenu' => 'type_top_menu',
                        'htmlOptions' => array('class' => 'cf'),
                        'maxChildLevel' => 0,
                        'encodeLabel' => false,
                    ));
                ?>
            </div>
            <div class="right-top-box">
                <?php $this->widget('application.widgets.ToggleLanguageWidget'); ?>
            </div>
        </div>
    </div>
    <div class="header-bottom">
        <div class="container">
            <div class="header-data cf">
                <a href="/<?php echo Yii::app()->language ?>/" class="logo"><img src="/themes/application/images/app/logo.png"> </a>
                <div class="header-contacts">
                    <div class="cf">
                        <?php echo Yii::app()->Block->getBlockContent('header_phones'); ?>
                        <div class="box-contacts-btns">
                            <?php $h_email = Yii::app()->Block->getBlockContent('header_email'); ?>
                            <div class="email"><a href="mailto:<?php echo $h_email; ?>"><?php echo $h_email; ?></a></div>
                            <div class="contacts-btns">
                                <a href="#box-form-callback" class="btn-callback js-main-popup"><?php echo Yii::t('main-ui', 'Заказать звонок'); ?></a>
                                <span class="separator">/</span>
                                <a href="#box-form-contact-us" class="btn-feedback js-main-popup"><?php echo Yii::t('main-ui', 'Написать нам'); ?></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="main-menu cf navbar navbar-default" role="navigation">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Переключатель</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <div class="menu-title"><?php echo Yii::t('main-ui', 'Меню'); ?></div>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <?php
                        $this->widget('MenuWidget', array(
                            'rootItem' => Yii::app()->menu->all,
                            'typeMenu' => 'type_main_menu',
//                            'addItems' => array(
//                                array(
//                                    'sequence' => 1,
//                                    'menu' => Yii::app()->AddMenuItems->getAddMenuInteresting($visibleChildLevels = true)
//                                ),
//                                array(
//                                    'sequence' => 2,
//                                    'menu' => Yii::app()->AddMenuItems->getAddMenuTour($visibleChildLevels = true)
//                                ),
//                                array(
//                                    'sequence' => 5,
//                                    'menu' => Yii::app()->AddMenuItems->getAddMenuSouvenir($visibleChildLevels = true)
//                                )
//                            ),
                            'htmlOptions' => array('class' => 'nav navbar-nav main-menu-ul'), // корневой ul
                            'submenuHtmlOptions' => array('class' => 'dropdown-menu'), // все ul кроме корневого
                            'activeCssClass' => 'active', // активный li
                            'activateParents' => 'true', // добавлять активность не только для конечного раздела, но и для всех родителей
                            //'labelTemplate' => '{label}', // шаблон для подписи
                            'labelDropDownTemplate' => '{label} <b class="caret"></b>', // шаблон для подписи разделов, которых есть потомки
                            //'linkOptions' => array(), // атрибуты для ссылок
                            'linkDropDownOptions' => array('data-target' => '#', 'class' => 'dropdown-toggle', 'data-toggle' => 'dropdown'), // атрибуты для ссылок для разделов, у которых есть потомки
                            'linkDropDownOptionsSecondLevel' => array('data-target' => '#', 'data-toggle' => 'dropdown'), // атрибуты для ссылок для разделов, у которых есть потомки
                            //'itemOptions' => array(), // атрибуты для li
                            'itemDropDownOptions' => array('class' => 'dropdown'),  // атрибуты для li разделов, у которых есть потомки
                            'itemDropDownOptionsSecondLevel' => array('class' => 'dropdown-submenu'),
                            //  'itemDropDownOptionsThirdLevel' => array('class' => ''),
                            'maxChildLevel' => 2,
                            'encodeLabel' => false,
                        ));
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>