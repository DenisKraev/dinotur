<div class="box-select-tour">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="box-order-tour">
                    <h3 class="title"><?php echo Yii::t('main-ui', 'Хотите выбрать и заказать тур?'); ?></h3>
                    <p class="subtitle"><?php echo Yii::t('main-ui', 'Выберите один или несколько туров, в которых вы хотели бы побывать'); ?></p>
                    <a href="#box-form-tour-order" class="style-btn style-btn-big js-main-popup"><?php echo Yii::t('main-ui', 'Заказать'); ?></a>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="box-pick-up-tour">
                    <h3 class="title"><?php echo Yii::t('main-ui', 'Подобрать тур'); ?></h3>
                    <p class="subtitle"><?php echo Yii::t('main-ui', 'Заполните небольшую анкету и мы подберем подходящий именно вам тур'); ?></p>
                    <a href="#box-form-tour-selection" class="style-btn style-btn-big js-main-popup"><?php echo Yii::t('main-ui', 'Подобрать'); ?></a>
                </div>
            </div>
        </div>
    </div>
</div>