<div class="box-ask-question">
    <div class="container">
        <h2 class="title title-main"><?php echo Yii::t('main-ui', 'Задать вопрос'); ?></h2>
        <p class="text"><?php echo Yii::t('main-ui', 'У вас появились вопросы? Мы всегда готовы посоветовать <br> или подобрать интересный тур'); ?></p>
        <?php $this->widget('application.widgets.formAskQuestionWidget'); ?>
    </div>
</div>