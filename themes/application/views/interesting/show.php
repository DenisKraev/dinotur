<div class="container inner-content">

    <div class="interesting-show">
        <?php foreach ($model as $modelItem) { ?>

            <?php ($modelItem->page_title) ? $this->setPageTitle($modelItem->page_title) : $this->setPageTitle($modelItem->name.' | '.Yii::app()->name);?>
            <?php $this->setKeywords($modelItem->meta_keywords); ?>
            <?php $this->setDescription($modelItem->meta_description); ?>

            <h2 class="title-page"><?php echo $modelItem->name; ?></h2>
            <div class="content">
                <?php echo $modelItem->content; ?>
            </div>
            <div class="archive"><?php echo CHtml::link('«&nbsp;'.Yii::t('main-ui', 'К списку статей'), array('index'), array('class' => 'brown-btn'))?></div>

        <?php } ?>
        <?php $this->widget('application.widgets.TourInterestingAssignmentWidget', array('selectedId'=>$modelItem->id_app_interesting)); ?>
    </div>

</div>