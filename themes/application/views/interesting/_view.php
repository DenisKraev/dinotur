<?php foreach($data->Interesting as $item){ ?>

    <div class="box-single-item ib">
        <a href="<?php echo $item->url; ?>" class="box-img">
            <img src="<?php echo $item->getImagePreview('_main_image')->getUrlPath() ?>" alt="<?php echo AppHelper::cutQuotes($item->name); ?>"/>
        </a>
        <h3 class="name"><a href="<?php echo $item->url; ?>" title="<?php echo AppHelper::cutQuotes($item->name); ?>"><?php echo AppHelper::Truncate($item->name, 45); ?></a></h3>
        <div class="description"><?php echo AppHelper::Truncate($item->getContentShort(), 220); ?></div>
        <div class="more">
            <a class="brown-btn" href="<?php echo $item->url; ?>"><?php echo Yii::t('main-ui', 'Подробнее'); ?></a>
        </div>
    </div>

<?php } ?>