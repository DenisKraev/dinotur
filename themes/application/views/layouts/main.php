<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php echo Yii::app()->language; ?>">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="content-language" content="<?php echo Yii::app()->language; ?>" >
<?php
  if (YII_DEBUG) {
    Yii::app()->assetManager->publish(YII_PATH.'/web/js/source', false, -1, true);
  }
?>
    <link rel="apple-touch-icon" href="/content/touch-icon-ipad-retina.png"/>
    <link rel="apple-touch-icon" sizes="72x72" href="/content/touch-icon-ipad.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="/content/touch-icon-iphone-retina.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="/content/touch-icon-ipad-retina.png" />
    <meta property="og:image" content="/content/touch-icon-ipad-retina.png">
    <link rel="image_src" href="/content/touch-icon-ipad-retina.png">

<?php
    Yii::app()->clientScript->registerCoreScript('bootstrap');

    Yii::app()->clientScript->registerScriptFile('/themes/application/js/lib/jquery.placeholder.js', CClientScript::POS_HEAD);
    Yii::app()->clientScript->registerScriptFile('/themes/application/js/lib/jquery.fancybox.pack.js', CClientScript::POS_HEAD);
    Yii::app()->clientScript->registerScriptFile('/themes/application/js/lib/jquery.fancybox-media.js', CClientScript::POS_HEAD);
    Yii::app()->clientScript->registerScriptFile('/themes/application/js/lib/jquery.fancybox-thumbs.js', CClientScript::POS_HEAD);
    Yii::app()->clientScript->registerScriptFile('/themes/application/js/lib/bootstrap-datepicker.js', CClientScript::POS_HEAD);
    Yii::app()->clientScript->registerScriptFile('/themes/application/js/lib/locales/bootstrap-datepicker.ru.js', CClientScript::POS_HEAD);
    Yii::app()->clientScript->registerScriptFile('/themes/application/js/lib/bootstrap-hover-dropdown.js', CClientScript::POS_HEAD);
    Yii::app()->clientScript->registerScriptFile('/themes/application/js/lib/jquery.lightSlider.min.js', CClientScript::POS_HEAD);
    Yii::app()->clientScript->registerScriptFile('/themes/application/js/lib/jquery.formstyler.js', CClientScript::POS_HEAD);
    Yii::app()->clientScript->registerScriptFile('/themes/application/js/lib/viewportchecker.js', CClientScript::POS_HEAD);
    Yii::app()->clientScript->registerScriptFile('/themes/application/js/lib/device.min.js', CClientScript::POS_HEAD);
    Yii::app()->clientScript->registerScriptFile('/themes/application/js/lib/respond.js', CClientScript::POS_HEAD);
    Yii::app()->clientScript->registerScriptFile('/themes/application/js/app.js', CClientScript::POS_HEAD);

    Yii::app()->clientScript->registerCssFile('/themes/application/css/lib/jquery.fancybox.css');
    Yii::app()->clientScript->registerCssFile('/themes/application/css/lib/jquery.fancybox-thumbs.css');
    Yii::app()->clientScript->registerCssFile('/themes/application/css/lib/lightSlider.css');
    Yii::app()->clientScript->registerCssFile('/themes/application/css/lib/jquery.formstyler.css');
    Yii::app()->clientScript->registerCssFile('/themes/application/css/lib/datepicker3.css');
    Yii::app()->clientScript->registerCssFile('/themes/application/css/normalize.css');
    Yii::app()->clientScript->registerCssFile('/themes/application/css/global/layout.css');
    Yii::app()->clientScript->registerCssFile('/themes/application/css/content.css');
    Yii::app()->clientScript->registerCssFile('/themes/application/css/app/app.css');
?>
    <script src="http://api-maps.yandex.ru/2.1/?load=package.full&lang=ru-RU" type="text/javascript"></script>
    <title><?php echo CHtml::encode($this->getPageTitle()); ?></title>
</head>
<body class="<?php echo 'controller_'.Yii::app()->controller->id.' '.'action_'.Yii::app()->controller->action->id; ?>">
    <div id="wrapper">
        <?php $this->renderPartial('webroot.themes.application.views.partials.Header') ?>

        <?php if (Yii::app()->controller->id == 'static') { ?>
            <div class="container inner-content">
        <?php } ?>
        <?php echo $content; ?>
        <?php if (Yii::app()->controller->id == 'static') { ?>
            </div>
        <?php } ?>

        <div class="cf"></div>
        <div id="push"></div>
    </div>
    <?php $this->renderPartial('webroot.themes.application.views.partials.Footer'); ?>

    <?php $this->widget('application.widgets.formContactUsWidget'); ?>
    <?php $this->widget('application.widgets.formCallbackWidget'); ?>
    <?php $this->widget('application.widgets.formTourOrderWidget'); ?>
    <?php $this->widget('application.widgets.FormTourSelectionWidget'); ?>
    <?php $this->widget('application.widgets.FormRespondWidget'); ?>
    <div id="box-map" style="min-height: 100%; display: none;"></div>
    <script>
        $(document).ready(function(){
            $('.style-form-checkbox, .style-form-select').styler({
                selectPlaceholder: '',
                selectSmartPositioning: true,
                onFormStyled: function(){
                    $('.box-popup').hide();
                }
            });

            $('.style-form-datepicker').datepicker({
                language: '<?php echo Yii::app()->language; ?>',
                autoclose: true
            });
        });

    </script>
    <!-- Yandex.Metrika counter --><script type="text/javascript">(function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter26528286 = new Ya.Metrika({id:26528286, webvisor:true, clickmap:true, trackLinks:true, accurateTrackBounce:true}); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="//mc.yandex.ru/watch/26528286" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->
</body>
</html>