<div class="container inner-content">

    <div class="box-albums">

        <div class="title-page title-photo-video">
            <span class="active"><?php echo Yii::t('main-ui', 'Фотоальбомы'); ?></span>
            <span class="separator">/</span>
            <a href="/<?php echo Yii::app()->language; ?>/videoalbum/"><?php echo Yii::t('main-ui', 'Видеоальбом'); ?></a>
        </div>

        <div class="box-album-list">
            <?php foreach($model as $modelItem){
                $this->renderPartial('_view', array('model' => $modelItem, 'limitPhoto' => 8, 'watermarkThumb' => $watermarkThumb, 'watermark' => $watermark));
            } ?>
        </div>

        <?php  $this->widget('LinkPagerWidget', array(
            'pages' => $pages,
        )); ?>

    </div>

</div>