<div class="box-single-item ib">
    <a href="<?php echo $data->url; ?>" class="box-img">
        <?php if($data->new_item) { ?>
            <div class="ribbon_<?php echo Yii::app()->language; ?>"></div>
        <?php } ?>
        <img src="<?php echo $data->getImagePreview('_main_image')->getUrlPath() ?>" alt="<?php echo AppHelper::cutQuotes($data->name); ?>"/>
    </a>
    <h3 class="name"><a href="<?php echo $data->url; ?>" title="<?php echo AppHelper::cutQuotes($data->name); ?>"><?php echo AppHelper::Truncate($data->name, 45); ?></a></h3>
    <div class="duration"><?php echo Yii::t('main-ui', '{n} день|{n} дня|{n} дней|{n} дня', $data->duration); ?></div>
    <div class="description"><?php echo AppHelper::Truncate($data->description, 220); ?></div>
    <div class="more">
        <a class="brown-btn" href="<?php echo $data->url; ?>"><?php echo Yii::t('main-ui', 'Подробнее'); ?></a>
    </div>
</div>