<?php
echo '<strong>Фамилия Имя: </strong>'.$data->name.'<br>';
echo '<strong>Планируемая дата прибытия: </strong>'.Yii::app()->dateFormatter->formatDateTime($data->planned_date, 'long', null).'<br>';
echo '<strong>Планируемое количество дней пребывания: </strong>'.$data->days_stay.'<br>';
echo '<strong>Из какого населенного пункта Вы планируете выезд: </strong>'.$data->town_out.'<br>';
echo '<strong>Каким образом планируете добраться: </strong>'.$data->plan_get.'<br>';
echo '<strong>Сколько вам полных лет: </strong>'.$data->full_years.'<br>';
echo '<strong>Сколько человек в вашей группе и какого они возраста: </strong>'.$data->composition_group.'<br>';
echo '<strong>Сколько детей в возрасте до 5 лет: </strong>'.$data->young_children.'<br>';
$data->pets_with = $data->pets_with == 1 ? "Да" : "Нет";
$data->insurance_with = $data->insurance_with == 1 ? "Да" : "Нет";
echo '<strong>Имеются ли с собой домашние животные: </strong>'.$data->pets_with.'<br>';
echo '<strong>Имеется ли у Вас страховка от несчастных случаев: </strong>'.$data->insurance_with.'<br>';
echo '<strong>Какой вид туризма вы предпочитаете: </strong>'.$data->tourism_prefer.'<br>';
echo '<strong>Вы что-то уже выбрали из наших туров: </strong>'.$data->our_tours.'<br>';
echo '<strong>Что бы Вы еще хотели увидеть, Ваши пожелания: </strong>'.$data->wishes.'<br>';
echo '<strong>Ваш контактный телефон для обратной связи: </strong>'.$data->phone.'<br>';
echo '<strong>Если есть дополнительный контактный номер: </strong>'.$data->additional_phone.'<br>';
echo '<strong>Ваш адрес электронной почты: </strong>'.$data->email.'<br>';