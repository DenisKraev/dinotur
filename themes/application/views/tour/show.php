<div class="container inner-content">

    <div class="tour-show">
        <?php foreach ($model as $modelItem) { ?>
            <?php $this->pageTitle=$modelItem->name.' | '.Yii::app()->name;  ?>

            <h2 class="title-page"><?php echo $modelItem->name; ?></h2>
            <div class="row">
                <div class="col-md-<?php echo (empty($modelItem->gallery)) ? '12' : '6' ?>">
                    <div class="item-parameter">
                        <h4 class="title"><?php echo Yii::t('main-ui', 'Описание тура') ?>:</h4>
                        <div class="text"><?php echo $modelItem->description; ?></div>
                    </div>
                    <div class="item-parameter price-box">
                        <h4 class="title"><?php echo Yii::t('main-ui', 'Стоимость на 1 человека') ?>:</h4>
                        <?php if($modelItem->getPriceGroup() !== null) { ?>
                            <div class="price-item">
                                <div class="label-price ib"><?php echo Yii::t('main-ui', 'Группа') ?></div>
                                <div class="ib price"><?php echo $modelItem->getPriceGroup(); ?></div>
                            </div>
                        <?php } ?>
                        <?php if($modelItem->price_children !== null) { ?>
                            <div class="price-item">
                                <div class="label-price ib"><?php echo Yii::t('main-ui', 'Дети') ?></div>
                                <div class="ib price"><?php echo $modelItem->price_children; ?> <?php echo Yii::t('main-ui', 'руб') ?>.</div>
                            </div>
                        <?php } ?>
                        <?php if($modelItem->price_adult !== null) { ?>
                            <div class="price-item">
                                <div class="label-price ib"><?php echo Yii::t('main-ui', 'Взрослые') ?></div>
                                <div class="ib price"><?php echo $modelItem->price_adult; ?> <?php echo Yii::t('main-ui', 'руб') ?>.</div>
                            </div>
                        <?php } ?>
                    </div>
                    <?php if($modelItem->getPriceIncluded() !== null) { ?>
                        <div class="item-parameter">
                            <h4 class="title"><?php echo Yii::t('main-ui', 'В стоимость включено') ?>:</h4>
                            <div class="text"><?php echo $modelItem->getPriceIncluded(); ?></div>
                        </div>
                    <?php } ?>
                    <?php if(!empty($modelItem->note)) { ?>
                        <div class="item-parameter">
                            <h4 class="title"><?php echo Yii::t('main-ui', 'Примечание') ?>:</h4>
                            <div class="text"><?php echo $modelItem->note; ?></div>
                        </div>
                    <?php } ?>
                </div>
                <?php if(!empty($modelItem->gallery)) { ?>
                    <div class="col-md-6">
                        <div class="tour-gallery gallery-hidden">
                            <?php foreach($modelItem->gallery as $imageItem => $imageValue ) { ?>

                                <?php
                                    $full = $imageValue->getPreview(1200, '', '_full', '', 90, '', array('file' => $watermark,'align_y' => 'bottom', 'align_x' => 'right'));
                                    $big_thumb = $imageValue->getPreview(450, 300, '_big_thumb', 'center', 90, '', array('file' => $watermark,'align_y' => 'bottom', 'align_x' => 'right'));
                                    $small = $imageValue->getPreview(120, 75, '_thumb', 'center', 90, '', array('file' => $watermarkThumb,'align_y' => 'bottom', 'align_x' => 'right'));
                                ?>
                                <div data-thumb="<?php echo $small->getUrlPath(); ?>" class="img-big-thumb">
                                    <a href="<?php echo $full->getUrlPath(); ?>" class="js-tour-gallery">
                                        <img src="<?php echo $big_thumb->getUrlPath(); ?>" />
                                    </a>
                                </div>

                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="box-tour-content">

                <div class="row">
                    <?php echo $modelItem->getContentTour(); ?>
                </div>

            </div>

        <?php } ?>

        <div class="box-order-tour-btn">
            <a href="#box-form-tour-order" class="style-btn style-btn-big js-main-popup"><?php echo Yii::t('main-ui', 'Заказать тур') ?></a>
        </div>

        <?php $this->widget('application.widgets.ReviewWidget', array('selectedId'=>$model[0]->id_app_tour)); ?>

    </div>
</div>