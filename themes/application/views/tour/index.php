<?php $this->pageTitle = Yii::t('main-ui', 'Наши туры').' | '.Yii::app()->name;  ?>

<div class="container inner-content">
    <h1 class="title-page"><?php echo Yii::t('main-ui', 'Наши туры'); ?></h1>

    <div class="box-filter">
        <?php echo CHtml::beginForm(CHtml::normalizeUrl(array('/tour/index')), 'get', array('id'=>'filter-form')); ?>

            <label for="category_all" class="brown-btn filter-btn all-filter <?php echo $selectAll ? 'active' : ''; ?>">
                <?php echo Yii::t('main-ui', 'Все'); ?>
                <?php echo CHtml::checkBox('category[]', $selectAll, array('id'=>'category_all', 'value' => '*', 'class' => ($selectAll == true) ? 'checked' : '')); ?>
            </label>

            <?php
                echo CHtml::checkBoxList(
                    'category',
                    (isset($_GET['category'])) ? $_GET['category'] : '',
                    $arrayTypeTour,
                    array(
                        'template' => '{beginLabel}{labelTitle}{input}{endLabel}',
                        'class' => 'categoryFilter',
                        'labelOptions' => array('class' => 'filter-btn brown-btn'),
                        'separator' => ''
                    )
                );
            ?>

        <?php echo CHtml::endForm(); ?>
    </div>

    <?php
        //$dataProvider->getPagination()->pageSize = 3;
        $this->widget('zii.widgets.CListView', array(
            'dataProvider'=>$dataProvider,
            'itemView'=>'_view',
            'id'=>'ajaxListView',
            'enablePagination'=>false,
            'template'=>"{items}",
            'itemsCssClass' => 'box-list-items items',
//            'pager'=>array(
//                'class'=>'LinkPagerWidget'
//            ),
        ));
    ?>

</div>

<div class="box-select-tour">
    <div class="box-pick-up-tour">
        <h3 class="title"><?php echo Yii::t('main-ui', 'Подобрать тур'); ?></h3>
        <p class="subtitle"><?php echo Yii::t('main-ui', 'Заполните небольшую анкету и мы подберем подходящий именно вам тур'); ?></p>
        <a href="#box-form-tour-selection" class="style-btn style-btn-big js-main-popup"><?php echo Yii::t('main-ui', 'Подобрать'); ?></a>
    </div>
</div>