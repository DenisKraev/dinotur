<div class="container inner-content">

    <div class="box-contact">
        <h2 class="title-page"><?php echo Yii::t('main-ui', 'Контакты'); ?></h2>
        <?php echo $model[0]->content; ?>
    </div>

</div>

<div class="box-form-contact">
    <div class="container">
        <?php $this->widget('application.widgets.formAskQuestionWidget'); ?>
    </div>
</div>