<div class="box-single-souvenir ib">
    <a href="<?php echo $data->url; ?>" class="box-img">
        <?php if ($data->getImagePreview('_main_image')) { ?>
            <img src="<?php echo $data->getImagePreview('_main_image')->getUrlPath() ?>" alt="<?php echo AppHelper::cutQuotes($data->name); ?>"/>
        <?php } else { ?>
            <img src="/content/no-image.jpg" />
        <?php } ?>
    </a>
    <h3 class="name"><a href="<?php echo $data->url; ?>" title="<?php echo $data->name; ?>"><?php echo HText::smartCrop($data->name, 40); ?></a></h3>
    <div class="price"><?php echo Yii::t('main-ui', 'Цена'); ?>: <span><?php echo $data->price.' '.Yii::t('main-ui', 'руб'); ?> </span></div>
    <div class="order">
        <a href="#box-form-souvenir-order" class="brown-btn js-main-popup" data-id-souvenir="<?php echo $data->id_app_souvenir; ?>"><?php echo Yii::t('main-ui', 'Заказать'); ?></a>
    </div>
</div>