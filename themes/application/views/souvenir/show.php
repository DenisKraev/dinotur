<div class="container inner-content">

    <div class="souvenir-show">
        <?php foreach ($model as $modelItem) { ?>
            <?php $this->pageTitle=$modelItem->name.' | '.Yii::t('main-ui', 'Сувениры').' | '.Yii::app()->name;  ?>

            <h2 class="title-page"><?php echo $modelItem->name; ?></h2>
            <div class="row">
                <div class="col-md-<?php echo (empty($modelItem->gallery)) ? '12' : '6' ?>">
                    <div class="item-parameter">
                        <h4 class="title"><?php echo Yii::t('main-ui', 'Описание') ?>:</h4>
                        <div class="text"><?php echo $modelItem->description; ?></div>
                    </div>
                    <div class="item-parameter price-box">
                        <h4 class="title"><?php echo Yii::t('main-ui', 'Параметры') ?>:</h4>
                        <div class="label-price ib"><?php echo Yii::t('main-ui', 'Цена'); ?>:</div>
                        <div class="ib price"><?php echo $modelItem->price.' '.Yii::t('main-ui', 'руб'); ?>.</div>
                        <?php if($modelItem->getParameters() !== null) {
                            echo $modelItem->getParameters(); } ?>
                    </div>
                </div>
                <?php if(!empty($modelItem->gallery)) { ?>
                    <div class="col-md-6">
                        <div class="souvenir-gallery gallery-hidden">
                            <?php foreach($modelItem->gallery as $imageItem => $imageValue ) { ?>

                                <?php
                                    $full = $imageValue->getPreview(1200, '', '_full', '', 90, '', array('file' => $watermark,'align_y' => 'bottom', 'align_x' => 'right'));
                                    $big_thumb = $imageValue->getPreview(450, 300, '_big_thumb', '', 90, '', array('file' => $watermarkThumb,'align_y' => 'bottom', 'align_x' => 'right'));
                                    $small = $imageValue->getPreview(120, 75, '_thumb', '', 90, '', array('file' => $watermarkThumb,'align_y' => 'bottom', 'align_x' => 'right'));
                                ?>
                                <div data-thumb="<?php echo $small->getUrlPath(); ?>" class="img-big-thumb">
                                    <a href="<?php echo $full->getUrlPath(); ?>" class="js-tour-gallery">
                                        <img src="<?php echo $big_thumb->getUrlPath(); ?>" />
                                    </a>
                                </div>

                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
            </div>

        <?php } ?>

        <div class="box-order-tour-btn">
            <a href="#box-form-souvenir-order" class="style-btn style-btn-big js-main-popup" data-id-souvenir="<?php echo $model[0]->id_app_souvenir ?>"><?php echo Yii::t('main-ui', 'Заказать сувенир') ?></a>
        </div>

    </div>
</div>
<?php $this->widget('application.widgets.FormSouvenirOrderWidget'); ?>