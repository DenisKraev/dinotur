<?php $this->pageTitle = Yii::t('main-ui', 'Сувениры').' | '.Yii::app()->name;  ?>
<?php //$this->setKeywords(); ?>
<?php //$this->setPageTitle(); ?>
<?php //$this->setDescription(); ?>

<div class="container inner-content">
    <h1 class="title-page"><?php echo Yii::t('main-ui', 'Сувениры'); ?></h1>

    <div class="box-filter">
        <?php echo CHtml::beginForm(CHtml::normalizeUrl(array('/souvenir/index')), 'get', array('id'=>'filter-form')); ?>

        <label for="category_all" class="brown-btn filter-btn all-filter <?php echo $selectAll ? 'active' : ''; ?>">
            <?php echo Yii::t('main-ui', 'Все'); ?>
            <?php echo CHtml::checkBox('category[]', $selectAll, array('id'=>'category_all', 'value' => '*', 'class' => ($selectAll == true) ? 'checked' : '')); ?>
        </label>

        <?php
        echo CHtml::checkBoxList(
            'category',
            (isset($_GET['category'])) ? $_GET['category'] : '',
            $arrayTypeSouvenir,
            array(
                'template' => '{beginLabel}{labelTitle}{input}{endLabel}',
                'class' => 'categoryFilter',
                'labelOptions' => array('class' => 'filter-btn brown-btn'),
                'separator' => ''
            )
        );
        ?>

        <?php echo CHtml::endForm(); ?>
    </div>

    <?php
    $dataProvider->getPagination()->pageSize = 52;
    $this->widget('zii.widgets.CListView', array(
        'dataProvider'=>$dataProvider,
        'itemView'=>'_view',
        'id'=>'ajaxListView',
        'enablePagination'=>true,
        'template'=>"{items}{pager}",
        'itemsCssClass' => 'box-souvenir items',
        'pager'=>array(
            'class'=>'LinkPagerWidget'
        ),
    ));
    ?>

</div>
<?php $this->widget('application.widgets.FormSouvenirOrderWidget'); ?>