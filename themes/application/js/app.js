$(document).ready(function(){

    $('.tour-gallery, .souvenir-gallery').lightSlider({
        gallery: true,
        loop: true,
        item:1,
        thumbItem: 4,
        thumbMargin: 5,
        mode: 'fade',
        speed: 300,
        currentPagerPosition: 'middle',
        onSliderLoad: function() {
            $('.tour-gallery, .souvenir-gallery').removeClass('gallery-hidden');
        }
    });

    // инициализация формстайлера находится в body

    // инициализация датапикера находится в body

    $('input, textarea').placeholder();

//    if (device.mobile() !== true || device.tablet() !== true) {
//        $('.dropdown-toggle').dropdownHover().data('toggle', '').attr('data-toggle', '');
//    }

    $(".js-main-popup").fancybox({
        padding: '0',
        width : 'auto',
        height : 'auto',
        maxWidth: "90%",
        fitToView: false,
        scrolling: 'no',
        helpers: {  overlay: { locked: false } },
        beforeLoad: function(){
            idSouvenir = this.element.data('id-souvenir');
            if (idSouvenir) {
                form = this.content;
                $.ajax({
                    url: "/ru/souvenir/souvenirbyid/",
                    data: { idsouvenir: idSouvenir },
                    type: "POST",
                    dataType: 'json',
                    success: function(data) {
                        form.find('.form-souvenir-id-souvenir').val(idSouvenir);
                        form.find('.form-souvenir-name-souvenir').val(data.idsouvenir[0].name_ru);
                        form.find('.name-title span').text(data.idsouvenir[0].name_ru);
                        form.find('.img-souvenir img').attr('src', data.idsouvenir.img);
                        form.find('.form-souvenir-price').val(data.idsouvenir[0].price);
                        form.find('.price-title span').text(data.idsouvenir[0].price);
                        form.find('.form-souvenir-parameters').val(data.idsouvenir[0].parameters_ru);
                    }
                });
            }
        },
        afterClose: function(){
            if (this.content.hasClass('box-popup-text') == true) {
                this.content.show();
            }
        }
    });
    $('.popup-cancel').click(function(){
        $.fancybox.close();
    });

    $('.dino').addClass("hid").viewportChecker({
        classToAdd: 'visi animated rotateInDownRight',
        offset: 200,
        repeat: false
    });

    function getUrlVars(){
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        var h0,h1;
        for(var i = 0; i < hashes.length; i++)
        {
            hash = hashes[i].split('=');

            h0 = decodeURIComponent(hash[0]).replace(/\+/g, ' ');
            h0 = h0.replace(/\[]/g, ''); // убираем квадратные скобки
            h1 = (hash[1] ? decodeURIComponent(hash[1].replace(/\+/g, ' ')) : hash[1]);
            if (vars[h0]) {
                if (!$.isArray(vars[h0])) vars[h0] = [vars[h0]];
                vars[h0].push(h1);
            } else {
                vars[h0] = h1;
            }
        }
        return vars;
    }

    changeFilter = function(){
        $('.categoryFilter').click(function(){
            // работает как радио кнопка
            $('.categoryFilter').prop("checked", false).parent('label').removeClass('active');
            $(this).prop('checked', true).parent('label').addClass('active');
            // Проверка на чекнутость и добавление класса
//            if($(this).prop('checked')){
//                $(this).parent('label').addClass('active');
//            }
//            else {
//                $(this).parent('label').removeClass('active');
//            }
            // клик по категории убирает активность с кнопки Все
            $('#category_all').removeClass("checked").prop("checked", false).parent('label').removeClass('active');
            category = $('.categoryFilter').serialize();
            if (category == '') {category = 'category%5B%5D=0'}
            $.fn.yiiListView.update(
                'ajaxListView',
                {data: category}
            );
            window.history.pushState(null, null, '?'+category);

        });
    }

    // при первоначальной загрузке страницы присваиваем классы
    filterCssInit = function(){
        $('.categoryFilter').each(function(){
            if($(this).prop('checked')){
                $(this).parent('label').addClass('active');
            }
            else {
                $(this).parent('label').removeClass('active');
            }
        });
    }

    $('#category_all').click(function(){
        if ($('#category_all').hasClass("checked")) {
            //при вторичном клике на кноку "Все" оставляем ее активной и навсякий случай убираем класс с кнопок категорий
            $('#category_all').prop("checked", true).parent('label').addClass('active');
            $('.categoryFilter').parent('label').removeClass('active');
        }
        else {
            $('#category_all').addClass("checked").parent('label').addClass('active');
            // при клике на некативную кнопку "Все" убираем активность со всех категорий
            $('.categoryFilter').prop("checked", false).parent('label').removeClass('active');
            category = $('#category_all').serialize();
            if (category == '') {category = 'category%5B%5D=0'}
            $.fn.yiiListView.update(
                'ajaxListView',
                {data: category}
            );
             window.history.pushState(null, null, '?'+category);
        }
    });

    filterCssInit();
    changeFilter();

    $(window).bind('popstate', function() {

        var params   = window.location.search;
        $.fn.yiiListView.update(
            'ajaxListView',
            {data: location.pathname + params}
        );

        $('.filter-btn').removeClass('active');

        if (params){
            idCatUrl = [];
            idCatUrl = getUrlVars();

            if (idCatUrl.category !== '*'){
                idCatPage = [];
                $('.categoryFilter').each(function(){
                    el = $(this).attr('id').replace(/category_/, '');
                    idCatPage.push(el);
                });
                idActive = [];
                var i = 0;
                $.grep(idCatUrl.category, function(el) {
                    var el1 = el;
                    parseInt(el1);
                    el1 = el1 - 1;
                    if ($.inArray(el1, idCatPage) == -1){
                        idActive.push(el1);
                        $('#category_'+el1).parent('label').addClass('active');
                    }
                    i++;
                });
            } else {
                $('#category_all').parent('label').addClass('active');
            }
        } else {
            category = 'category%5B%5D=*'
            $.fn.yiiListView.update(
                'ajaxListView',
                {data: category}
            );
            $('#category_all').parent('label').addClass('active');
        }
    });

//    function isHhistoryApiAvailable() {
//        return !!(window.history && history.pushState);
//    }
//
//    alert(isHhistoryApiAvailable());

    var boxen = [];
    $("a[class^=js-tour-gallery], a[class^=js-popup-img]").each(function() {
        if ($.inArray($(this).attr('class'),boxen)) boxen.push($(this).attr('class'));
    });
    $(boxen).each(function(i,val) {
        $('a[class='+val+']').attr('rel',val).fancybox({
            fitToView		: false,
            padding		: '0',
            helpers : {overlay: {locked: false}}
        });
    });

    $('.js-tour-gallery, .js-popup-img').fancybox({
        openEffect : 'elastic',
        openSpeed  : 150,
        closeEffect : 'elastic',
        closeSpeed  : 150,
        closeBtn  : true,
        arrows    : true,
        nextClick : true,
        helpers : {
            buttons	: {},
            thumbs : {
                width  : 90,
                height : 50
            },
            beforeShow : function() {
                var s='';
                var alt = this.element.find('img').attr('alt');
                var title = this.element.find('img').attr('title');
                var ze = (title == undefined || title=='') ? true : false;
                var te = (alt == undefined || alt=='') ? true : false;
                if (ze) {title = this.title;}
                if (alt == title) {title='';}
                var ze= (title == undefined || title=='') ? true : false;
                if (!ze && !te) s=alt + '<br>' + title;
                if (ze && !te) s=alt;
                if (!ze && te) s=title;
                this.title = s;
            },
            overlay: {
                locked: false
            }
        },
        afterLoad : function(){if (this.group.length>1){this.title = 'Изображение ' + (this.index + 1) + ' из ' + this.group.length+(this.title ? ' - ' + this.title : '');}
        else{this.helpers.buttons = false;return;}}
    });

    $(".js-popup-video").fancybox({
        type: 'iframe'
    });

    $('.js-get-form-review-btn').click(function(){
        $('.box-form-review').slideToggle();
        $(this).siblings('.arrow').toggleClass('open');
    });

});



