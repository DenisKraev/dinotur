(function( $ ) {

    var methods = {

        /* init
         *=================================================================*/
        init : function( options ) {

            $.each( options.images, function(index,value){

                var img = new Image();
                img.src = value;

                img.onload = function() {

                    options.counter++;

                    if( options.counter >= options.total ) { options.callback.call(); }

                } // onload END

            }); // each END

        } // init END

    }; // method END

    preloadImage = function( options ) {

        // Default options
        var settings = $.extend( {
            'minPreload'      : -1,
            callback         : function() {}
        }, options );

        var options = settings;

        options.total       = options.images.length;
        options.counter     = 0;

        if( options.minPreload == -1 ) { options.minPreload = options.total;	}

        methods.init( options );
    };
})( jQuery );
