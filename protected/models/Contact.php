<?php

/**
 * Модель для таблицы "app_contact".
 *
 * The followings are the available columns in table 'app_contact':
 * @property integer $id_app_contact
 * @property string $content_ru
 * @property string $content_en
 */
class Contact extends DaActiveRecord {

  const ID_OBJECT = 'project-kontakty';

  protected $idObject = self::ID_OBJECT;

  /**
   * Returns the static model of the specified AR class.
   * @param string $className active record class name.
   * @return Contact the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  /**
   * @return string the associated database table name
   */
  public function tableName() {
    return 'app_contact';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    return array(
      array('content_ru', 'required'),
      array('content_en', 'safe'),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    return array(
    );
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
      'id_app_contact' => 'ID',
      'content_ru' => 'Содержание',
      'content_en' => 'Содержание тура (по английски)',
    );
  }

    private function getAttributeByLanguage($attr)  {
        $attr .= '_'.Yii::app()->language;
        return $attr;
    }

    public function getContent()  {
        $attr = $this->getAttributeByLanguage('content');
        return $this->$attr;
    }

}