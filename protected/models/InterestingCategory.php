<?php

/**
 * Модель для таблицы "app_interesting_category".
 *
 * The followings are the available columns in table 'app_interesting_category':
 * @property integer $id_app_interesting_category
 * @property string $name_ru
 * @property string $name_en
 * @property integer $sequence
 * @property integer $visible
 *
 * The followings are the available model relations:
 * @property AppInteresting[] $appInterestings
 */
class InterestingCategory extends DaActiveRecord {

  const ID_OBJECT = 'project-kategorii-eto-interesno';

  protected $idObject = self::ID_OBJECT;

  /**
   * Returns the static model of the specified AR class.
   * @param string $className active record class name.
   * @return InterestingCategory the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  /**
   * @return string the associated database table name
   */
  public function tableName() {
    return 'app_interesting_category';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    return array(
      array('name_ru', 'required'),
      array('sequence, visible', 'numerical', 'integerOnly'=>true),
      array('name_ru, name_en', 'length', 'max'=>255),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    return array(
      //'Interesting' => array(self::HAS_MANY, 'Interesting', 'id_app_interesting_category'),
      'Interesting' => array(self::MANY_MANY,
          'Interesting',
          'app_interesting_category_assignment(id_app_interesting_category, id_app_interesting)',
          'order' => 'Interesting_Interesting.sequence ASC'
      ),
    );
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
      'id_app_interesting_category' => 'ID',
      'name_ru' => 'Название',
      'name_en' => 'Название (по английски)',
      'sequence' => 'п/п',
      'visible' => 'Видимость',
    );
  }

    private function getAttributeByLanguage($attr)  {
        $attr .= '_'.Yii::app()->language;
        return $attr;
    }

    public function getName()  {
        $attr = $this->getAttributeByLanguage('name');
        return $this->$attr;
    }

}