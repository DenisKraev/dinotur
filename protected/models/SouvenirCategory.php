<?php

/**
 * Модель для таблицы "app_souvenir_category".
 *
 * The followings are the available columns in table 'app_souvenir_category':
 * @property integer $id_app_souvenir_category
 * @property string $name_ru
 * @property string $name_en
 * @property integer $visible
 * @property integer $sequence
 *
 * The followings are the available model relations:
 * @property AppSouvenir[] $appSouvenirs
 */
class SouvenirCategory extends DaActiveRecord {

  const ID_OBJECT = 'project-kategorii-suvenirov';

  protected $idObject = self::ID_OBJECT;

  /**
   * Returns the static model of the specified AR class.
   * @param string $className active record class name.
   * @return SouvenirCategory the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  /**
   * @return string the associated database table name
   */
  public function tableName() {
    return 'app_souvenir_category';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    return array(
      array('name_ru', 'required'),
      array('visible, sequence', 'numerical', 'integerOnly'=>true),
      array('name_ru, name_en', 'length', 'max'=>255),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    return array(
      'Souvenir' => array(self::HAS_MANY, 'Souvenir', 'id_app_souvenir_category'),
    );
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
      'id_app_souvenir_category' => 'ID',
      'name_ru' => 'Название',
      'name_en' => 'Название (по английски)',
      'visible' => 'Видимость',
      'sequence' => 'п/п',
    );
  }

    private function getAttributeByLanguage($attr)  {
        $attr .= '_'.Yii::app()->language;
        return $attr;
    }

    public function getName()  {
        $attr = $this->getAttributeByLanguage('name');
        return $this->$attr;
    }

}