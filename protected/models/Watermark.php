<?php

/**
 * Модель для таблицы "app_watermark".
 *
 * The followings are the available columns in table 'app_watermark':
 * @property integer $id_app_watermark
 * @property integer $watermark_image
 * @property integer $id_object
 *
 * The followings are the available model relations:
 * @property File $watermarkImage
 */
class Watermark extends DaActiveRecord {

  const ID_OBJECT = 'project-vodnye-znaki';
  const ID_PARAMETER_WATERMARK_IMAGE = 'project-vodnye-znaki-watermark-image';

  protected $idObject = self::ID_OBJECT;
  public $idParameterWatermarkImage = self::ID_PARAMETER_WATERMARK_IMAGE;

  /**
   * Returns the static model of the specified AR class.
   * @param string $className active record class name.
   * @return Watermark the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  /**
   * @return string the associated database table name
   */
  public function tableName() {
    return 'app_watermark';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    return array(
      array('watermark_image, id_object', 'required'),
      array('watermark_image, id_object', 'numerical', 'integerOnly'=>true),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    return array(
      'watermarkImage' => array(self::HAS_ONE, 'File',
          array(
              'id_object' => 'idObject',
              'id_instance' => 'id_app_watermark',
              'id_parameter' => 'idParameterWatermarkImage'
          ),),
    );
  }

    public function behaviors() {
        $behaviors = array(
            'ImagePreviewBehavior' => array(
                'class' => 'ImagePreviewBehavior',
                'imageProperty' => 'watermarkImage',
                'formats' => array('_orig'=>array()),
            ),
        );
        return $behaviors;
    }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
      'id_app_watermark' => 'ID',
      'watermark_image' => 'Водный знак',
      'id_object' => 'Применить к',
    );
  }

}