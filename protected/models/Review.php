<?php

/**
 * Модель для таблицы "app_review".
 *
 * The followings are the available columns in table 'app_review':
 * @property integer $id_app_review
 * @property integer $id_app_tour
 * @property string $name_sender
 * @property string $text
 * @property string $datetime
 * @property integer $accepted
 *
 * The followings are the available model relations:
 * @property AppTour $appTour
 */
class Review extends DaActiveRecord {

  const ID_OBJECT = 'project-otzyvy';

  protected $idObject = self::ID_OBJECT;

  /**
   * Returns the static model of the specified AR class.
   * @param string $className active record class name.
   * @return Review the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

    public function init() {
        parent::init();
        $this->datetime = time();
    }

  /**
   * @return string the associated database table name
   */
  public function tableName() {
    return 'app_review';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    return array(
      array('name_sender, text', 'required'),
      array('id_app_tour, accepted', 'numerical', 'integerOnly'=>true),
      array('name_sender', 'length', 'max'=>255),
      array('datetime', 'length', 'max'=>10),
      array('text', 'length', 'max'=>1000),
      array('name_sender, text', 'filter', 'filter' => array('CHtml', 'encode')),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    return array(
      'Tour' => array(self::BELONGS_TO, 'Tour', 'id_app_tour'),
    );
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
      'id_app_review' => 'ID',
      'id_app_tour' => 'Тур',
      'name_sender' => Yii::t('main-ui', 'Имя отправителя'),
      'text' => Yii::t('main-ui', 'Текст отзыва'),
      'datetime' => 'Дата отзыва',
      'accepted' => 'Принято модератором',
    );
  }

}