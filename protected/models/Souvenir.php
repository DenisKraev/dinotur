<?php

/**
 * Модель для таблицы "app_souvenir".
 *
 * The followings are the available columns in table 'app_souvenir':
 * @property integer $id_app_souvenir
 * @property integer $id_app_souvenir_category
 * @property string $name_ru
 * @property string $name_en
 * @property string $alias
 * @property string $price
 * @property integer $main_image
 * @property string $description_ru
 * @property string $description_en
 * @property string $parameters_ru
 * @property string $parameters_en
 * @property integer $visible
 * @property integer $sequence
 *
 * The followings are the available model relations:
 * @property AppSouvenirCategory $appSouvenirCategory
 * @property File $mainImage
 */
class Souvenir extends DaActiveRecord {

    const ID_OBJECT = 'project-suveniry';
    const ID_PARAMETER_MAIN_IMAGE = 'project-suveniry-main-image';
    const ID_PARAMETER_GALLERY = 'project-suveniry-galereya';
    const ID_WATERMARK = 1;
    const ID_WATERMARK_THUMB = 2;

    protected $idObject = self::ID_OBJECT;
    public $idParameterMainImage = self::ID_PARAMETER_MAIN_IMAGE;
    public $idParameterGallery = self::ID_PARAMETER_GALLERY;
    public $idWatermark = self::ID_WATERMARK;
    public $idWatermarkThumb = self::ID_WATERMARK_THUMB;

  /**
   * Returns the static model of the specified AR class.
   * @param string $className active record class name.
   * @return Souvenir the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  /**
   * @return string the associated database table name
   */
  public function tableName() {
    return 'app_souvenir';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    return array(
      array('id_app_souvenir_category, name_ru, alias, price', 'required'),
      array('id_app_souvenir_category, main_image, visible, sequence', 'numerical', 'integerOnly'=>true),
      array('name_ru, name_en, alias, price', 'length', 'max'=>255),
      array('description_ru, description_en, parameters_ru, parameters_en', 'safe'),
    );
  }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
            'SouvenirCategory' => array(self::BELONGS_TO, 'SouvenirCategory', 'id_app_souvenir_category'),
            'mainImage' => array(
                self::HAS_ONE, 'File',
                array(
                    'id_object' => 'idObject',
                    'id_instance' => 'id_app_souvenir',
                    'id_parameter' => 'idParameterMainImage'
                ),
            ),
            'gallery' => array(
                self::HAS_MANY, 'File',
                array(
                    'id_object' => 'idObject',
                    'id_instance' => 'id_app_souvenir',
                    'id_parameter' => 'idParameterGallery'
                ),
                'condition' => '`id_parent_file` IS NULL'
            ),
        );
    }

    public function behaviors() {
        $behaviors = array(
            'ImagePreviewBehavior' => array(
                'class' => 'ImagePreviewBehavior',
                'imageProperty' => 'mainImage',
                'formats' => array(
                    '_main_image' => array(
                        'width' => '205',
                        'height' => '205',
                        //'crop' => 'center',
                        //'resize' => true,
                        'watermark' =>  array(
                            'file' => $this->getWatermark(self::ID_WATERMARK_THUMB),
                            'align_y' => 'bottom',
                            'align_x' => 'right',
                        )
                    ),
                ),
            ),
        );
        return $behaviors;
    }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
      'id_app_souvenir' => 'ID',
      'id_app_souvenir_category' => 'Категория сувенира',
      'name_ru' => 'Название',
      'name_en' => 'Название (по английски)',
      'alias' => 'В адресной строке',
      'price' => 'Цена',
      'main_image' => 'Главная картинка',
      'description_ru' => 'Описание',
      'description_en' => 'Описание (по английски)',
      'parameters_ru' => 'Параметры',
      'parameters_en' => 'Параметры (по английски)',
      'visible' => 'Видимость',
      'sequence' => 'п/п',
    );
  }

    private function getAttributeByLanguage($attr)  {
        $attr .= '_'.Yii::app()->language;
        return $attr;
    }

    public function getName()  {
        $attr = $this->getAttributeByLanguage('name');
        return $this->$attr;
    }

    public function getDescription()  {
        $attr = $this->getAttributeByLanguage('description');
        return $this->$attr;
    }

    public function getParameters()  {
        $attr = $this->getAttributeByLanguage('parameters');
        return $this->$attr;
    }

    public function getUrl() {
        if ($this->alias) {
            return Yii::app()->createUrl('souvenir/'.$this->alias);
        }
        return Yii::app()->createUrl('souvenir/index');
    }

    public function getWatermark($id) {
        $model = Watermark::model()->find('id_object=:id_object', array(':id_object'=>$id));
        if(!empty($model->watermarkImage)) {
            return $model->getImagePreview('_orig')->getUrlPath();
        }
    }

    protected function beforeSave() {
        $text = trim($this->alias);
        $this->alias = HText::translit($text, '_', false);
        return parent::beforeSave();
    }

}