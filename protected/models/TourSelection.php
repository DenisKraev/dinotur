<?php

/**
 * Модель для таблицы "app_tour_selection".
 *
 * The followings are the available columns in table 'app_tour_selection':
 * @property integer $id_app_tour_selection
 * @property string $name
 * @property string $planned_date
 * @property integer $days_stay
 * @property string $town_out
 * @property string $plan_get
 * @property integer $full_years
 * @property string $composition_group
 * @property integer $young_children
 * @property integer $pets_with
 * @property integer $insurance_with
 * @property string $tourism_prefer
 * @property string $our_tours
 * @property string $wishes
 * @property string $phone
 * @property string $additional_phone
 * @property string $email
 * @property string $datetime
 * @property integer $processed
 */
class TourSelection extends DaActiveRecord {

  const ID_OBJECT = 'project-podbor-tura';

  protected $idObject = self::ID_OBJECT;

  /**
   * Returns the static model of the specified AR class.
   * @param string $className active record class name.
   * @return TourSelection the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

    public function init() {
        parent::init();
        $this->datetime = time();
    }

  /**
   * @return string the associated database table name
   */
  public function tableName() {
    return 'app_tour_selection';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    return array(
      array('name, email', 'required'),
      array('days_stay, full_years, young_children, pets_with, insurance_with, processed', 'numerical', 'integerOnly'=>true),
      array('name, town_out, phone, additional_phone, email', 'length', 'max'=>255),
      array('planned_date, datetime', 'length', 'max'=>10),
      array('plan_get, composition_group, tourism_prefer, our_tours, wishes', 'safe'),
      array('wishes, plan_get, composition_group, tourism_prefer', 'length', 'max'=>1000),
      array('wishes, plan_get, composition_group, tourism_prefer, name, email', 'filter', 'filter' => array('CHtml', 'encode')),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    return array(
    );
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
      'id_app_tour_selection' => 'ID',
      'name' => Yii::t('main-ui', 'Фамилия Имя'),
      'planned_date' => Yii::t('main-ui', 'Планируемая дата прибытия'),
      'days_stay' => Yii::t('main-ui', 'Планируемое количество дней пребывания'),
      'town_out' => Yii::t('main-ui', 'Из какого населенного пункта Вы планируете выезд'),
      'plan_get' => Yii::t('main-ui', 'Каким образом планируете добраться'),
      'full_years' => Yii::t('main-ui', 'Сколько вам полных лет'),
      'composition_group' => Yii::t('main-ui', 'Сколько человек в вашей группе и какого они возраста'),
      'young_children' => Yii::t('main-ui', 'Сколько детей в возрасте до 5 лет'),
      'pets_with' => Yii::t('main-ui', 'Имеются ли с собой домашние животные'),
      'insurance_with' => Yii::t('main-ui', 'Имеется ли у Вас страховка от несчастных случаев'),
      'tourism_prefer' => Yii::t('main-ui', 'Какой вид туризма вы предпочитаете'),
      'our_tours' => Yii::t('main-ui', 'Вы что-то уже выбрали из наших туров'),
      'wishes' => Yii::t('main-ui', 'Что бы Вы еще хотели увидеть, Ваши пожелания'),
      'phone' => Yii::t('main-ui', 'Ваш контактный телефон для обратной связи'),
      'additional_phone' => Yii::t('main-ui', 'Если есть дополнительный контактный номер'),
      'email' => Yii::t('main-ui', 'Ваш адрес электронной почты'),
      'datetime' => 'Дата заявки',
      'processed' => 'Обработано',
    );
  }

    public function getTourismPrefer(){
        return $result = array(
            'Оздоровительный' => Yii::t('main-ui', 'Оздоровительный'),
            'Экскурсионно-позновательный' => Yii::t('main-ui', 'Экскурсионно-позновательный'),
            'Активный' => Yii::t('main-ui', 'Активный'),
            'Спортивный' => Yii::t('main-ui', 'Спортивный'),
            'Экстремальный' => Yii::t('main-ui', 'Экстремальный'),
            'Религиозный' => Yii::t('main-ui', 'Религиозный'),
            'Приключенческий' => Yii::t('main-ui', 'Приключенческий'),
            'Деловой' => Yii::t('main-ui', 'Деловой'),
            'Зеленый' => Yii::t('main-ui', 'Зеленый'),
        );
    }

}