<?php

/**
 * Модель для таблицы "app_photoalbum".
 *
 * The followings are the available columns in table 'app_photoalbum':
 * @property integer $id_app_photoalbum
 * @property string $name_ru
 * @property string $name_en
 * @property integer $visible
 * @property integer $sequence
 */
class Photoalbum extends DaActiveRecord {

  const ID_OBJECT = 'project-fotoalbomy';
  const ID_WATERMARK = 1;
  const ID_WATERMARK_THUMB = 2;

  protected $idObject = self::ID_OBJECT;
  public $idWatermark = self::ID_WATERMARK;
  public $idWatermarkThumb = self::ID_WATERMARK_THUMB;

  /**
   * Returns the static model of the specified AR class.
   * @param string $className active record class name.
   * @return Photoalbum the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  /**
   * @return string the associated database table name
   */
  public function tableName() {
    return 'app_photoalbum';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    return array(
      array('name_ru', 'required'),
      array('visible, sequence', 'numerical', 'integerOnly'=>true),
      array('name_ru, name_en', 'length', 'max'=>255),
    );
  }

  /**
   * @return array relational rules.
   */
//  public function relations() {
//    return array(
//    );
//  }

    public function behaviors() {
        return array(
            'photos' => array(
                'class' => 'application.behaviors.PhotosBehavior',
                'idObject' => $this->getIdObject(),
            ),
        );
    }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
      'id_app_photoalbum' => 'ID',
      'name_ru' => 'Название альбома',
      'name_en' => 'Название альбома (по английски)',
      'visible' => 'Видимость',
      'sequence' => 'п/п',
    );
  }

    private function getAttributeByLanguage($attr)  {
        $attr .= '_'.Yii::app()->language;
        return $attr;
    }

    public function getName()  {
        $attr = $this->getAttributeByLanguage('name');
        return $this->$attr;
    }

    public function getWatermark($id) {
        $model = Watermark::model()->find('id_object=:id_object', array(':id_object'=>$id));
        if(!empty($model->watermarkImage)) {
            return $model->getImagePreview('_orig')->getUrlPath();
        }
    }

    protected function beforeSave() {
        $text = trim($this->alias);
        $this->alias = HText::translit($text, '_', false);
        return parent::beforeSave();
    }

}