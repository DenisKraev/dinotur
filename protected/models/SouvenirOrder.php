<?php

/**
 * Модель для таблицы "app_souvenir_order".
 *
 * The followings are the available columns in table 'app_souvenir_order':
 * @property integer $id_app_souvenir_order
 * @property string $name_souvenir
 * @property integer $price
 * @property string $parameters
 * @property string $name_sender
 * @property string $email
 * @property string $phone
 * @property string $datetime
 * @property string $address
 * @property integer $processed
 * @property integer $id_app_souvenir
 *
 * The followings are the available model relations:
 * @property AppSouvenir $appSouvenir
 */
class SouvenirOrder extends DaActiveRecord {

  const ID_OBJECT = 'project-zakaz-suvenira';

  protected $idObject = self::ID_OBJECT;

  /**
   * Returns the static model of the specified AR class.
   * @param string $className active record class name.
   * @return SouvenirOrder the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

    public function init() {
        parent::init();
        $this->datetime = time();
    }

  /**
   * @return string the associated database table name
   */
  public function tableName() {
    return 'app_souvenir_order';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    return array(
      array('name_sender, email', 'required'),
      array('price, processed, id_app_souvenir', 'numerical', 'integerOnly'=>true),
      array('name_souvenir, name_sender, email, phone', 'length', 'max'=>255),
      array('datetime', 'length', 'max'=>10),
      array('address', 'safe'),
      array('address', 'length', 'max'=>1000),
      array('name_sender, email, phone, address', 'filter', 'filter' => array('CHtml', 'encode')),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    return array(
      'Souvenir' => array(self::BELONGS_TO, 'Souvenir', 'id_app_souvenir'),
    );
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
      'id_app_souvenir_order' => 'ID',
      'name_souvenir' => 'Название',
      'price' => 'Цена',
      'parameters' => 'Параметры',
      'name_sender' => 'Имя заказчика',
      'email' => 'Email',
      'phone' => Yii::t('main-ui', 'Телефон'),
      'datetime' => 'Дата заказа',
      'address' => Yii::t('main-ui', 'Адрес'),
      'processed' => 'Обработан',
      'id_app_souvenir' => 'ID Сувенира',
    );
  }

}