<?php

/**
 * Модель для таблицы "app_tour_order".
 *
 * The followings are the available columns in table 'app_tour_order':
 * @property integer $id_app_tour_order
 * @property string $selected_tours
 * @property string $wishes
 * @property integer $adults
 * @property integer $young_children
 * @property integer $older_children
 * @property string $name
 * @property string $email
 * @property string $datetime
 */
class TourOrder extends DaActiveRecord {

  const ID_OBJECT = 'project-zakazy-turov';

  protected $idObject = self::ID_OBJECT;

  /**
   * Returns the static model of the specified AR class.
   * @param string $className active record class name.
   * @return TourOrder the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

    public function init() {
        parent::init();
        $this->datetime = time();
    }

  /**
   * @return string the associated database table name
   */
  public function tableName() {
    return 'app_tour_order';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    return array(
      array('selected_tours, name, email', 'required'),
      array('adults, young_children, older_children', 'numerical', 'integerOnly'=>true),
      array('name, email', 'length', 'max'=>255),
      array('datetime', 'length', 'max'=>10),
      array('wishes, phone', 'safe'),
      array('wishes', 'length', 'max'=>1000),
      array('wishes, email, phone, name', 'filter', 'filter' => array('CHtml', 'encode')),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    return array(
    );
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
      'id_app_tour_order' => 'ID',
      'selected_tours' => 'Туры',
      'wishes' => 'Пожелания',
      'adults' => Yii::t('main-ui', 'Состав группы. Взрослых').':',
      'young_children' => Yii::t('main-ui', 'Дети до 5 лет').':',
      'older_children' => Yii::t('main-ui', 'Дети старше 5 лет').':',
      'name' => Yii::t('main-ui', 'Ваше имя').':',
      'email' => Yii::t('main-ui', 'Ваш e-mail').':',
      'phone' => Yii::t('main-ui', 'Ваш телефон').':',
      'datetime' => 'Дата заказа',
    );
  }

    public function getCountParticipants($max_count){
        $result = array();
        $more = array($max_count + 1 => '> '.$max_count);
        for ($i = 1; $i <= $max_count; $i++) {
            $result[$i] = $i;
        }
        return $result + $more;
    }

}