<?php

/**
 * Модель для таблицы "app_interesting".
 *
 * The followings are the available columns in table 'app_interesting':
 * @property integer $id_app_interesting
 * @property integer $id_app_interesting_category
 * @property integer $main_image
 * @property string $name_ru
 * @property string $name_en
 * @property string $alias
 * @property string $content_short_ru
 * @property string $content_short_en
 * @property string $content_ru
 * @property string $content_en
 * @property integer $sequence
 * @property integer $visible
 *
 * The followings are the available model relations:
 * @property AppInterestingCategory $appInterestingCategory
 * @property File $mainImage
 * @property AppTourInterestingAssignment[] $appTourInterestingAssignments
 */
class Interesting extends DaActiveRecord {

  const ID_OBJECT = 'project-eto-interesno';
  const ID_PARAMETER_MAIN_IMAGE = 'project-eto-interesno-main-image';
  const ID_WATERMARK = 1;
  const ID_WATERMARK_THUMB = 2;

  protected $idObject = self::ID_OBJECT;
  public $idParameterMainImage = self::ID_PARAMETER_MAIN_IMAGE;
  public $idWatermark = self::ID_WATERMARK;
  public $idWatermarkThumb = self::ID_WATERMARK_THUMB;

  /**
   * Returns the static model of the specified AR class.
   * @param string $className active record class name.
   * @return Interesting the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  /**
   * @return string the associated database table name
   */
  public function tableName() {
    return 'app_interesting';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    return array(
      array('main_image, name_ru, alias, content_short_ru, content_ru', 'required'),
      array('main_image, visible', 'numerical', 'integerOnly'=>true),
      array('name_ru, name_en, alias', 'length', 'max'=>255),
      array('content_short_en, content_en', 'safe'),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    return array(
      //'InterestingCategory' => array(self::BELONGS_TO, 'InterestingCategory', 'id_app_interesting_category'),
      'InterestingCategory' => array(self::MANY_MANY, 'InterestingCategory', 'app_interesting_category_assignment(id_app_interesting, id_app_interesting_category)'),
        'mainImage' => array(
            self::HAS_ONE, 'File',
            array(
                'id_object' => 'idObject',
                'id_instance' => 'id_app_interesting',
                'id_parameter' => 'idParameterMainImage'
            ),
        ),
      'Tour' => array(self::MANY_MANY, 'Tour', 'app_tour_interesting_assignment(id_app_interesting, id_app_tour)')
    );
  }

    public function behaviors() {
        $behaviors = array(
            'ImagePreviewBehavior' => array(
                'class' => 'ImagePreviewBehavior',
                'imageProperty' => 'mainImage',
                'formats' => array(
                    '_main_image' => array(
                        'width' => '299',
                        'height' => '218',
                        'crop' => 'center',
                        //'resize' => true
                        'watermark' =>  array(
                            'file' => $this->getWatermark(self::ID_WATERMARK_THUMB),
                            'align_y' => 'bottom',
                            'align_x' => 'right',
                        )
                    ),
                ),
            ),
        );
        return $behaviors;
    }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
      'id_app_interesting' => 'ID',
      'id_app_interesting_category' => 'Категория это интересно',
      'main_image' => 'Главная картинка',
      'name_ru' => 'Название',
      'name_en' => 'Название (по английски)',
      'alias' => 'В адресной строке',
      'content_short_ru' => 'Краткое содержание',
      'content_short_en' => 'Кратное содержание (по английски)',
      'content_ru' => 'Содержание',
      'content_en' => 'Содержание (по английски)',
      //'sequence' => 'п/п',
      'visible' => 'Видимость',
    );
  }

    private function getAttributeByLanguage($attr)  {
        $attr .= '_'.Yii::app()->language;
        return $attr;
    }

    public function getName()  {
        $attr = $this->getAttributeByLanguage('name');
        return $this->$attr;
    }

    public function getContentShort()  {
        $attr = $this->getAttributeByLanguage('content_short');
        return $this->$attr;
    }

    public function getContent()  {
        $attr = $this->getAttributeByLanguage('content');
        return $this->$attr;
    }

    public function getUrl() {
        if ($this->alias) {
            return Yii::app()->createUrl('/interesting/'.$this->alias);
        }
        return Yii::app()->createUrl('interesting/index');
    }

    public function getWatermark($id) {
        $model = Watermark::model()->find('id_object=:id_object', array(':id_object'=>$id));
        if(!empty($model->watermarkImage)) {
            return $model->getImagePreview('_orig')->getUrlPath();
        }
    }

    protected function beforeSave() {
        $text = trim($this->alias);
        $this->alias = HText::translit($text, '_', false);
        return parent::beforeSave();
    }

}