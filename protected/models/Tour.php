<?php

/**
 * Модель для таблицы "app_tour".
 *
 * The followings are the available columns in table 'app_tour':
 * @property integer $id_app_tour
 * @property integer $id_app_tour_category
 * @property string $name_ru
 * @property string $name_en
 * @property integer $duration
 * @property string $description_ru
 * @property string $description_en
 * @property string $price_group_ru
 * @property string $price_group_en
 * @property integer $price_children
 * @property integer $price_adult
 * @property string $price_included_ru
 * @property string $price_included_en
 * @property string $note_ru
 * @property string $note_en
 * @property string $content_tour_ru
 * @property string $content_tour_en
 * @property integer $main_image
 * @property integer $sequence
 * @property integer $state
 *
 * The followings are the available model relations:
 * @property AppTourCategory $appTourCategory
 * @property File $mainImage
 * @property AppReview[] $appReviews
 */
class Tour extends DaActiveRecord {

  const ID_OBJECT = 'project-tury';
  const ID_PARAMETER_MAIN_IMAGE = 'project-tury-main-image';
  const ID_PARAMETER_GALLERY = 'project-tury-galereya';
  const ID_WATERMARK = 1;
  const ID_WATERMARK_THUMB = 2;

  protected $idObject = self::ID_OBJECT;
  public $idParameterMainImage = self::ID_PARAMETER_MAIN_IMAGE;
  public $idParameterGallery = self::ID_PARAMETER_GALLERY;
  public $idWatermark = self::ID_WATERMARK;
  public $idWatermarkThumb = self::ID_WATERMARK_THUMB;

  /**
   * Returns the static model of the specified AR class.
   * @param string $className active record class name.
   * @return Tour the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  /**
   * @return string the associated database table name
   */
  public function tableName() {
    return 'app_tour';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    return array(
      array('id_app_tour_category, name_ru, duration, content_tour_ru, main_image', 'required'),
      array('id_app_tour_category, duration, price_children, price_adult, main_image, sequence, state', 'numerical', 'integerOnly'=>true),
      array('name_ru, name_en, price_group_ru, price_group_en', 'length', 'max'=>255),
      array('description_ru, description_en, price_included_ru, price_included_en, note_ru, note_en, content_tour_en', 'safe'),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    return array(
      'Interesting' => array(self::MANY_MANY, 'Interesting', 'app_tour_interesting_assignment(id_app_tour, id_app_interesting)'),
      'TourCategory' => array(self::BELONGS_TO, 'TourCategory', 'id_app_tour_category'),
      'Review' => array(self::HAS_MANY, 'Review', 'id_app_tour'),
      'mainImage' => array(
          self::HAS_ONE, 'File',
          array(
              'id_object' => 'idObject',
              'id_instance' => 'id_app_tour',
              'id_parameter' => 'idParameterMainImage'
          ),
      ),
      'gallery' => array(
            self::HAS_MANY, 'File',
            array(
                'id_object' => 'idObject',
                'id_instance' => 'id_app_tour',
                'id_parameter' => 'idParameterGallery'
            ),
            'condition' => '`id_parent_file` IS NULL'
      ),
    );
  }

    public function behaviors() {
        $behaviors = array(
            'ImagePreviewBehavior' => array(
                'class' => 'ImagePreviewBehavior',
                'imageProperty' => 'mainImage',
                'formats' => array(
                    '_main_image' => array(
                        'width' => '299',
                        'height' => '218',
                        'crop' => 'center',
                        'watermark' =>  array(
                            'file' => $this->getWatermark(self::ID_WATERMARK_THUMB),
                            'align_y' => 'bottom',
                            'align_x' => 'right',
                        )
                    ),
                ),
            ),
        );
        return $behaviors;
    }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
      'id_app_tour' => 'ID',
      'id_app_tour_category' => 'Категория тура',
      'name_ru' => 'Название по русски',
      'name_en' => 'Название по английски',
      'duration' => 'Продолжительность тура',
      'description_ru' => 'Описание по русски',
      'description_en' => 'Описание по английски',
      'price_group_ru' => 'Стоимость группа (по русски)',
      'price_group_en' => 'Стоимость группа (по английски)',
      'price_children' => 'Стоимость дети (руб)',
      'price_adult' => 'Стоимость взрослые (руб)',
      'price_included_ru' => 'В стоимость включено (по русски)',
      'price_included_en' => 'В стоимость включено (по английски)',
      'note_ru' => 'Примечание (по русски)',
      'note_en' => 'Примечание (по английски)',
      'content_tour_ru' => 'Содержание тура (по русски)',
      'content_tour_en' => 'Содержание тура (по английски)',
      'main_image' => 'Главная картинка',
      'sequence' => 'п/п',
      'state' => 'Видимость',
    );
  }

    private function getAttributeByLanguage($attr)  {
        $attr .= '_'.Yii::app()->language;
        return $attr;
    }

    public function getName()  {
        $attr = $this->getAttributeByLanguage('name');
        return $this->$attr;
    }

    public function getDescription()  {
        $attr = $this->getAttributeByLanguage('description');
        return $this->$attr;
    }

    public function getPriceGroup()  {
        $attr = $this->getAttributeByLanguage('price_group');
        return $this->$attr;
    }
    public function getPriceIncluded()  {
        $attr = $this->getAttributeByLanguage('price_included');
        return $this->$attr;
    }
    public function getNote()  {
        $attr = $this->getAttributeByLanguage('note');
        return $this->$attr;
    }
    public function getContentTour()  {
        $attr = $this->getAttributeByLanguage('content_tour');
        return $this->$attr;
    }

    public function getUrl() {
        if ($this->alias) {
            return Yii::app()->createUrl('/tour/'.$this->alias);
        }
        return Yii::app()->createUrl('tour/index');
    }

    public function getWatermark($id) {
        $model = Watermark::model()->find('id_object=:id_object', array(':id_object'=>$id));
        if(!empty($model->watermarkImage)) {
            return $model->getImagePreview('_orig')->getUrlPath();
        }
    }

    protected function beforeSave() {
        $text = trim($this->alias);
        $this->alias = HText::translit($text, '_', false);
        return parent::beforeSave();
    }

}