<?php

/**
 * Модель для таблицы "app_interesting_category_assignment".
 *
 * The followings are the available columns in table 'app_interesting_category_assignment':
 * @property integer $id_app_interesting_category_assignment
 * @property integer $id_app_interesting
 * @property integer $id_app_interesting_category
 *
 * The followings are the available model relations:
 * @property AppInteresting $appInteresting
 * @property AppInterestingCategory $appInterestingCategory
 */
class InterestingCategoryAssignment extends DaActiveRecord {

  const ID_OBJECT = 'project-svyaz-eto-interesno-i-kategorii';

  protected $idObject = self::ID_OBJECT;

  /**
   * Returns the static model of the specified AR class.
   * @param string $className active record class name.
   * @return InterestingCategoryAssignment the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  /**
   * @return string the associated database table name
   */
  public function tableName() {
    return 'app_interesting_category_assignment';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    return array(
      array('id_app_interesting, id_app_interesting_category', 'required'),
      array('id_app_interesting, id_app_interesting_category', 'numerical', 'integerOnly'=>true),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    return array(
      'appInteresting' => array(self::BELONGS_TO, 'AppInteresting', 'id_app_interesting'),
      'appInterestingCategory' => array(self::BELONGS_TO, 'AppInterestingCategory', 'id_app_interesting_category'),
    );
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
      'id_app_interesting_category_assignment' => 'ID',
      'id_app_interesting' => 'Это интересно',
      'id_app_interesting_category' => 'Категория это интересно',
    );
  }

}