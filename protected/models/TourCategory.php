<?php

/**
 * Модель для таблицы "app_tour_category".
 *
 * The followings are the available columns in table 'app_tour_category':
 * @property integer $id_app_tour_category
 * @property string $name_ru
 * @property string $name_en
 * @property integer $sequence
 * @property integer $state
 *
 * The followings are the available model relations:
 * @property AppTour[] $appTours
 */
class TourCategory extends DaActiveRecord {

  const ID_OBJECT = 'project-kategorii-turov';

  protected $idObject = self::ID_OBJECT;

  /**
   * Returns the static model of the specified AR class.
   * @param string $className active record class name.
   * @return TourCategory the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  /**
   * @return string the associated database table name
   */
  public function tableName() {
    return 'app_tour_category';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    return array(
      array('name_ru', 'required'),
      array('sequence, state', 'numerical', 'integerOnly'=>true),
      array('name_ru, name_en', 'length', 'max'=>255),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    return array(
      'Tour' => array(self::HAS_MANY, 'Tour', 'id_app_tour_category'),
    );
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
      'id_app_tour_category' => 'ID',
      'name_ru' => 'Название по русски',
      'name_en' => 'Название по английски',
      'sequence' => 'п/п',
      'state' => 'Видимость',
    );
  }

    private function getAttributeByLanguage($attr)  {
        $attr .= '_'.Yii::app()->language;
        return $attr;
    }

    public function getName()  {
        $attr = $this->getAttributeByLanguage('name');
        return $this->$attr;
    }

}