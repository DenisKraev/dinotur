<?php

/**
 * Модель для таблицы "app_tour_interesting_assignment".
 *
 * The followings are the available columns in table 'app_tour_interesting_assignment':
 * @property integer $id_app_tour_interesting_assignment
 * @property integer $id_app_tour
 * @property integer $id_app_interesting
 */
class TourInterestingAssignment extends DaActiveRecord {

  const ID_OBJECT = 'project-svyaz-turov-i-eto-interesno';

  protected $idObject = self::ID_OBJECT;

  /**
   * Returns the static model of the specified AR class.
   * @param string $className active record class name.
   * @return TourInterestingAssignment the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  /**
   * @return string the associated database table name
   */
  public function tableName() {
    return 'app_tour_interesting_assignment';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    return array(
      array('id_app_tour, id_app_interesting', 'required'),
      array('id_app_tour, id_app_interesting', 'numerical', 'integerOnly'=>true),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    return array(
    );
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
      'id_app_tour_interesting_assignment' => 'ID',
      'id_app_tour' => 'ID',
      'id_app_interesting' => 'ID',
    );
  }

}