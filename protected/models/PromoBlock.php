<?php

/**
 * Модель для таблицы "app_promo_block".
 *
 * The followings are the available columns in table 'app_promo_block':
 * @property integer $id_app_promo_block
 * @property string $title_ru
 * @property string $url
 * @property integer $bg_image
 * @property integer $state
 * @property string $text_ru
 * @property string $text_en
 * @property string $title_en
 * @property integer $sequence
 *
 * The followings are the available model relations:
 * @property File $bgImage
 */
class PromoBlock extends DaActiveRecord {

  const ID_OBJECT = 'project-bloki---ssylki-na-glavnoi';
  const ID_PARAMETER_PROMOBLOCK_BG = 'project-bloki---ssylki-na-glavnoi-bg-image';

  protected $idObject = self::ID_OBJECT;
  public $idParameterPromoblockBg = self::ID_PARAMETER_PROMOBLOCK_BG;

  /**
   * Returns the static model of the specified AR class.
   * @param string $className active record class name.
   * @return PromoBlock the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  /**
   * @return string the associated database table name
   */
  public function tableName() {
    return 'app_promo_block';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    return array(
      array('bg_image, state, sequence', 'numerical', 'integerOnly'=>true),
      array('title_ru, url, title_en', 'length', 'max'=>255),
      array('text_ru, text_en', 'safe'),
    );
  }

  /**
   * @return array relational rules.
   */

    public function relations() {
        return array(
            'bgImage' => array(
                self::HAS_ONE, 'File',
                array(
                    'id_object' => 'idObject',
                    'id_instance' => 'id_app_promo_block',
                    'id_parameter' => 'idParameterPromoblockBg'
                ),
            ),
        );
    }

    public function behaviors() {
        $behaviors = array(
            'ImagePreviewBehavior' => array(
                'class' => 'ImagePreviewBehavior',
                'imageProperty' => 'bgImage',
                'formats' => array(
                    '_bg' => array(
                        'width' => '288'
                    ),
                ),
            ),
        );
        return $behaviors;
    }

    private function getAttributeByLanguage($attr)  {
        $attr .= '_'.Yii::app()->language;
        return $attr;
    }

    public function getTitle()  {
        $attr = $this->getAttributeByLanguage('title');
        return $this->$attr;
    }

    public function getText()  {
        $attr = $this->getAttributeByLanguage('text');
        return $this->$attr;
    }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
      'id_app_promo_block' => 'ID',
      'title_ru' => 'Заголовок по русски',
      'url' => 'URL ссылка',
      'bg_image' => 'Картинка на фон',
      'state' => 'Видимость',
      'text_ru' => 'Текст по русски',
      'text_en' => 'Текст по английски',
      'title_en' => 'Заголовок по английски',
      'sequence' => 'п/п',
    );
  }

}