<?php

class ContactController extends Controller
{
    const EVENT_TYPE_NEW_FORM_CALLBACK = 51; // id события в панели
    const EVENT_TYPE_NEW_FORM_FEEDBACK = 52; // id события в панели

    protected $urlAlias = "contact";

    public function actionIndex()
    {
        $model=Contact::model()->findAll();

        $this->render('index',array(
            'model'=>$model,
        ));
    }

    // форма вопроса
    public function actionAskQuestion() {

        $model = BaseActiveRecord::newModel('FormFeedback');
        $modelClass = get_class($model);
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'form-ask-question-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST[$modelClass])) {
            $model->attributes=$_POST[$modelClass];
            $model->onAfterSave = array($this, 'sendMessage');
            if ($model->save()) {
                Yii::app()->user->setFlash('form-feedback-success', Yii::t('main-ui', 'Спасибо за обращение. Ваше сообщение успешно отправлено'));
            } else {
                Yii::app()->user->setFlash('form-feedback-success', CHtml::errorSummary($model, '<p>Не удалось отправить форму</p>'));
            }
        }
        $this->redirect(Yii::app()->user->returnUrl);

    }

    // попап написать нам
    public function actionSendContactUs() {

        $model = BaseActiveRecord::newModel('FormContactUs');
        $modelClass = get_class($model);
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'form-contact-us-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST[$modelClass])) {
            $model->attributes=$_POST[$modelClass];
            $model->onAfterSave = array($this, 'sendMessage');
            if ($model->save()) {
                Yii::app()->user->setFlash('form-feedback-success', Yii::t('main-ui', 'Спасибо за обращение. Ваше сообщение успешно отправлено'));
            } else {
                Yii::app()->user->setFlash('form-feedback-success', CHtml::errorSummary($model, '<p>Не удалось отправить форму</p>'));
            }
        }
        $this->redirect(Yii::app()->user->returnUrl);

    }

    // попап заказа звонка
    public function actionSendCallback() {

        $model = BaseActiveRecord::newModel('FormCallback');
        $modelClass = get_class($model);
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'form-callback-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST[$modelClass])) {
            $model->attributes=$_POST[$modelClass];
            $model->onAfterSave = array($this, 'sendMessageCallback');
            if ($model->save()) {
                Yii::app()->user->setFlash('form-callback-success', Yii::t('main-ui', 'Спасибо за обращение. Ваше сообщение успешно отправлено'));
            } else {
                Yii::app()->user->setFlash('form-callback-success', CHtml::errorSummary($model, '<p>Не удалось отправить форму</p>'));
            }
        }
        $this->redirect(Yii::app()->user->returnUrl);

    }

    // отправка почты с формы связи, и попапа формы связи
    public function sendMessage(CEvent $event) {
        Yii::app()->notifier->addNewEvent(
            self::EVENT_TYPE_NEW_FORM_FEEDBACK,
            $this->renderPartial('feedback_email_body', array('feedback' => $event->sender), true)
        )->sendNowLastAdded();
    }

    // отправка почты с попапа заказа звонка
    public function sendMessageCallback(CEvent $event) {
        Yii::app()->notifier->addNewEvent(
            self::EVENT_TYPE_NEW_FORM_CALLBACK,
            $this->renderPartial('callback_email_body', array('callback' => $event->sender), true)
        )->sendNowLastAdded();
    }
}