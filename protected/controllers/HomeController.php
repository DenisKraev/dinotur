<?php

class HomeController extends Controller
{
    protected $urlAlias = "/";

	public function actionIndex()
	{
		$this->render('index');
	}

}