<?php

class VideoalbumController extends Controller
{

    public $urlAlias  = 'videoalbum';

	public function actionIndex()
	{
        $criteria = new CDbCriteria();

        $criteria->condition = 'visible=:visible';
        $criteria->params = array(':visible' => 1);
        $criteria->order = 'sequence ASC';

        $count=Videoalbum::model()->count($criteria);

        $pages=new CPagination($count);
        $pages->pageSize = 30;
        $pages->applyLimit($criteria);

        $model=Videoalbum::model()->findAll($criteria);

        $this->render('index',array(
            'model'=>$model,
            'pages' => $pages
        ));
	}

}
