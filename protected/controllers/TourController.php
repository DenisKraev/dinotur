<?php

class TourController extends Controller
{
    const EVENT_TYPE_NEW_FORM_ORDER_TOUR = 53;
    const EVENT_TYPE_NEW_FORM_REVIEW = 54;
    const EVENT_TYPE_NEW_FORM_SELECTION_TOUR = 56;

    public $urlAlias  = 'tour';

    public function actionIndex(array $category = array())
    {
        $criteria = new CDbCriteria();
        $selectAll = false;

        if(count( $category ) > 0) {

            if ($category[0] == '*'){
                $criteria->with = array(
                    'TourCategory' => array(
//                    'joinType'=>'INNER JOIN',
                        'order' => 't.sequence ASC',
                        'condition' => 't.state = 1 AND TourCategory.state = 1',
                    ),
                );
                $dataProvider = new CActiveDataProvider('Tour', array('criteria' => $criteria));
                $selectAll = true;
            }
            else {
                $criteria->with = array('TourCategory');
                $criteria->addInCondition('t.id_app_tour_category', $category);
                $criteria->addCondition('t.state = 1');
                $criteria->addCondition('TourCategory.state = 1');
                $criteria->order = 't.sequence ASC';
                $dataProvider = new CActiveDataProvider('Tour', array('criteria' => $criteria));
            }
        }
        else {

            $criteria->with = array(
                'TourCategory' => array(
//                    'joinType'=>'INNER JOIN',
                    'order' => 't.sequence ASC',
                    'condition' => 't.state = 1 AND TourCategory.state = 1',
                ),
            );
            $dataProvider = new CActiveDataProvider('Tour', array('criteria' => $criteria));
            $selectAll = true;
        }

        // формирование элементов для фильтра
        $criteria2 = new CDbCriteria();
        $criteria2->with = array('Tour');
        $criteria2->addCondition('t.state = 1');
        $criteria2->addCondition('Tour.state = 1');
        $criteria2->order = 't.sequence ASC';

        $modelTour = TourCategory::model()->findAll($criteria2);

        $arrayTypeTour = array();
        foreach($modelTour as $item) {
            $arrayTypeTour[$item->id_app_tour_category] = $item->name;
        }

        $this->render('index', array(
            'dataProvider' => $dataProvider,
            'arrayTypeTour' => $arrayTypeTour,
            'selectAll' => $selectAll
        ));
    }

    public function actionShow()
    {
        $criteria=new CDbCriteria;
        $criteria->condition = 'alias=:alias';
        $criteria->params = array(':alias'=>$_GET['id']);

        $model=Tour::model()->findAll($criteria);

        $watermark = Tour::model()->getWatermark(Tour::model()->idWatermark);
        $watermarkThumb = Tour::model()->getWatermark(Tour::model()->idWatermarkThumb);

        $this->render('show',array(
            'model'=>$model,
            'watermark'=>$watermark,
            'watermarkThumb'=>$watermarkThumb,
        ));
    }

    public function actionOrderTour()
    {
        $model = BaseActiveRecord::newModel('TourOrder');
        $modelClass = get_class($model);
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'form-tour-order-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST[$modelClass])) {
            $model->attributes=$_POST[$modelClass];
            $model->onAfterSave = array($this, 'sendOrderTour');
            $model->selected_tours = implode(", ", $model->selected_tours);
            if ($model->save()) {
                Yii::app()->user->setFlash('form-tour-order-form-success', 'Спасибо за обращение. Ваше сообщение успешно отправлено');
            } else {
                Yii::app()->user->setFlash('form-tour-order-form-error', CHtml::errorSummary($model, '<p>Не удалось отправить форму</p>'));
            }
        }
        $this->redirect(Yii::app()->user->returnUrl);
    }

    public function actionSelectionTour()
    {
        $model = BaseActiveRecord::newModel('TourSelection');
        $modelClass = get_class($model);
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'form-tour-selection-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST[$modelClass])) {
            $model->attributes=$_POST[$modelClass];
            $model->onAfterSave = array($this, 'sendSelectionTour');
            if ($model->our_tours !== null) {
                $model->our_tours = implode(", ", $model->our_tours);
            }
            if ($model->tourism_prefer !== null){
                $model->tourism_prefer = implode(", ", $model->tourism_prefer);
            }
            if ($model->planned_date !== null){
                $model->planned_date = strtotime($model->planned_date);
            }
            if ($model->save()) {
                Yii::app()->user->setFlash('form-tour-selection-form-success', 'Спасибо за обращение. Ваше сообщение успешно отправлено');
            } else {
                Yii::app()->user->setFlash('form-tour-selection-form-error', CHtml::errorSummary($model, '<p>Не удалось отправить форму</p>'));
            }
        }
        $this->redirect(Yii::app()->user->returnUrl);
    }

    public function actionWriteReview()
    {
        $model = BaseActiveRecord::newModel('Review');
        $modelClass = get_class($model);
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'form-review-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST[$modelClass])) {
            $model->attributes=$_POST[$modelClass];
            $model->onAfterSave = array($this, 'sendReview');
            if ($model->save()) {
                Yii::app()->user->setFlash('form-review-form-success', 'Спасибо. Ваш отзыв будет опубликован после модерации');
            } else {
                Yii::app()->user->setFlash('form-review-form-error', CHtml::errorSummary($model, '<p>Не удалось отправить форму</p>'));
            }
        }
        $this->redirect(Yii::app()->user->returnUrl);
    }

    public function sendOrderTour(CEvent $event) {
        Yii::app()->notifier->addNewEvent(
            self::EVENT_TYPE_NEW_FORM_ORDER_TOUR,
            $this->renderPartial('order_tour_email_body', array('data' => $event->sender), true)
        )->sendNowLastAdded();
    }

    public function sendSelectionTour(CEvent $event) {
        Yii::app()->notifier->addNewEvent(
            self::EVENT_TYPE_NEW_FORM_SELECTION_TOUR,
            $this->renderPartial('selection_tour_email_body', array('data' => $event->sender), true)
        )->sendNowLastAdded();
    }

    public function sendReview(CEvent $event) {
        Yii::app()->notifier->addNewEvent(
            self::EVENT_TYPE_NEW_FORM_REVIEW,
            $this->renderPartial('review_email_body', array('data' => $event->sender), true)
        )->sendNowLastAdded();
    }
}