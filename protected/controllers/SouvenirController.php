<?php

class SouvenirController extends Controller
{

    const EVENT_TYPE_NEW_FORM_ORDER_SOUVENIR = 55;

    public $urlAlias  = 'souvenir';

    public function actionIndex(array $category = array())
    {
        $criteria = new CDbCriteria();
        $selectAll = false;

        if(count( $category ) > 0) {

            if ($category[0] == '*'){
                $criteria->with = array(
                    'SouvenirCategory' => array(
//                    'joinType'=>'INNER JOIN',
                        'order' => 't.sequence ASC',
                        'condition' => 't.visible = 1 AND SouvenirCategory.visible = 1',
                    ),
                );
                $dataProvider = new CActiveDataProvider('Souvenir', array('criteria' => $criteria));
                $selectAll = true;
            }
            else {
                $criteria->with = array('SouvenirCategory');
                $criteria->addInCondition('t.id_app_souvenir_category', $category);
                $criteria->addCondition('t.visible = 1');
                $criteria->addCondition('SouvenirCategory.visible = 1');
                $criteria->order = 't.sequence ASC';
                $dataProvider = new CActiveDataProvider('Souvenir', array('criteria' => $criteria));
            }
        }
        else {

            $criteria->with = array(
                'SouvenirCategory' => array(
//                    'joinType'=>'INNER JOIN',
                    'order' => 't.sequence ASC',
                    'condition' => 't.visible = 1 AND SouvenirCategory.visible = 1',
                ),
            );
            $dataProvider = new CActiveDataProvider('Souvenir', array('criteria' => $criteria));
            $selectAll = true;
        }

        // формирование элементов для фильтра
        $criteria2 = new CDbCriteria();
        $criteria2->with = array('Souvenir');
        $criteria2->addCondition('t.visible = 1');
        $criteria2->addCondition('Souvenir.visible = 1');
        $criteria2->order = 't.sequence ASC';

        $model = SouvenirCategory::model()->findAll($criteria2);

        $arrayTypeSouvenir = array();
        foreach($model as $item) {
            $arrayTypeSouvenir[$item->id_app_souvenir_category] = $item->name;
        }

        $this->render('index', array(
            'dataProvider' => $dataProvider,
            'arrayTypeSouvenir' => $arrayTypeSouvenir,
            'selectAll' => $selectAll
        ));
    }

    public function actionShow()
    {
        $criteria=new CDbCriteria;
        $criteria->condition = 'alias=:alias';
        $criteria->params = array(':alias'=>$_GET['id']);

        $model=Souvenir::model()->findAll($criteria);

        $watermark = Photoalbum::model()->getWatermark(Photoalbum::model()->idWatermark);
        $watermarkThumb = Photoalbum::model()->getWatermark(Photoalbum::model()->idWatermarkThumb);

        $this->render('show',array(
            'model'=>$model,
            'watermark'=>$watermark,
            'watermarkThumb'=>$watermarkThumb,
        ));
    }

    public function actionOrderSouvenir()
    {
        $model = BaseActiveRecord::newModel('SouvenirOrder');
        $modelClass = get_class($model);
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'form-tour-souvenir-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST[$modelClass])) {
            $model->attributes=$_POST[$modelClass];
            $model->onAfterSave = array($this, 'sendOrderSouvenir');
            $model->parameters = preg_replace("/(\r\n)+/i", "\r\n", strip_tags($model->parameters));
            if ($model->save()) {
                Yii::app()->user->setFlash('form-souvenir-order-form-success', 'Спасибо за обращение. Ваше сообщение успешно отправлено');
            } else {
                Yii::app()->user->setFlash('form-souvenir-order-form-error', CHtml::errorSummary($model, '<p>Не удалось отправить форму</p>'));
            }
        }
        $this->redirect(Yii::app()->user->returnUrl);
    }

    public function actionSouvenirById() {
        if(isset($_POST['idsouvenir'])) {
            $model = Souvenir::model()->findAllByPk($_POST['idsouvenir']);
            if ($model[0]->getImagePreview('_main_image')) {
                $model = $model + array('img' => $model[0]->getImagePreview('_main_image')->getUrlPath());
            } else {
                $model = $model + array('img' => '/content/no-image.jpg');
            }
        }
        else {
            $model = NULL;
        }
        echo CJSON::encode(array('idsouvenir'=>$model));
    }

    public function sendOrderSouvenir(CEvent $event) {
        Yii::app()->notifier->addNewEvent(
            self::EVENT_TYPE_NEW_FORM_ORDER_SOUVENIR,
            $this->renderPartial('order_souvenir_email_body', array('data' => $event->sender), true)
        )->sendNowLastAdded();
    }
}