<?php

class InterestingController extends Controller
{

    public $urlAlias  = 'interesting';
    public function actionIndex(array $category = array())
    {
        $criteria = new CDbCriteria();
        $selectAll = false;

        if(count( $category ) > 0) {

            if ($category[0] == '*'){
                $criteria->with = array('Interesting');
                $criteria->addCondition('t.visible = 1');
                $criteria->addCondition('Interesting.visible = 1');
                //$criteria->order = 'Interesting.sequence ASC';
                $criteria->order = 't.sequence ASC';
                $criteria->together = true;
                $criteria->group = 'Interesting.id_app_interesting'; // для удаления дублей
                $dataProvider = new CActiveDataProvider('InterestingCategory', array('criteria' => $criteria));
                $selectAll = true;
            }
            else {

                $criteria->with = array('Interesting');
                $criteria->addInCondition('t.id_app_interesting_category', $category);
                $criteria->addCondition('t.visible = 1');
                $criteria->addCondition('Interesting.visible = 1');
                //$criteria->order = 'Interesting.sequence ASC';
                //$criteria->order = 't.sequence ASC';
                $criteria->together = true;
                //$criteria->distinct = true;
                $criteria->group = 'Interesting.id_app_interesting'; // для удаления дублей
                $dataProvider = new CActiveDataProvider('InterestingCategory', array('criteria' => $criteria));
            }
        }
        else {
            $criteria->with = array('Interesting');
            $criteria->addCondition('t.visible = 1');
            $criteria->addCondition('Interesting.visible = 1');
            //$criteria->order = 'Interesting.sequence ASC';
            $criteria->order = 't.sequence ASC';
            $criteria->together = true;
            $criteria->group = 'Interesting.id_app_interesting'; // для удаления дублей
            $dataProvider = new CActiveDataProvider('InterestingCategory', array('criteria' => $criteria));
            $selectAll = true;
        }

        // формирование элементов для фильтра
        $criteria2 = new CDbCriteria();
        $criteria2->with = array('Interesting');
        $criteria2->addCondition('t.visible = 1');
        $criteria2->addCondition('Interesting.visible = 1');
        $criteria2->order = 't.sequence ASC';

        $model = InterestingCategory::model()->findAll($criteria2);

        $arrayType = array();
        foreach($model as $item) {
            $arrayType[$item->id_app_interesting_category] = $item->name;
        }

        $this->render('index', array(
            'dataProvider' => $dataProvider,
            'arrayType' => $arrayType,
            'selectAll' => $selectAll
        ));
    }

    public function actionShow()
    {
        $criteria=new CDbCriteria;
        $criteria->condition = 'alias=:alias';
        $criteria->params = array(':alias'=>$_GET['id']);

        $model = Interesting::model()->findAll($criteria);

        $this->render('show',array(
            'model'=>$model,
        ));
    }
}
