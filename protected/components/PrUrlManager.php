<?php
Yii::import('application.extensions.urlManager.LangUrlManager', true);

class PrUrlManager extends LangUrlManager  {
    protected function createUrlRule($route,$pattern) {
        $language = $this->languages;
        return parent::createUrlRule($route, "<".$this->langParam.":(".implode('|',$language).")>/".$pattern);
    }
}