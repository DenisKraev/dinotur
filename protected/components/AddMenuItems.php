<?php

class AddMenuItems extends CWidget {

    public function getAddMenuTour($visibleChildLevels = true) {

        $criteria = new CDbCriteria();
        $criteria->with = array('Tour');
        $criteria->addCondition('t.state = 1');
        $criteria->addCondition('Tour.state = 1');
        $criteria->order = 't.sequence ASC';

        $model = TourCategory::model()->findAll($criteria);

        $arraySubItem = array();
        $arraySubItems = array();
        if(count($model) > 0 && $visibleChildLevels) {
            foreach($model as $k => $item) {
                $arraySubItem['label'] = $item->name;
                $arraySubItem['url'] = '/'.Yii::app()->language.'/tour/?category%5B%5D='.$item->id_app_tour_category;
                $arraySubItems[$k] = $arraySubItem;
            }
            $array = array(
                'active' => Yii::app()->controller->id == 'tour' ? 1 : '',
                'label' =>  Yii::t('main-ui', 'Наши туры').' <b class="caret"></b>',
                'url' => '/'.Yii::app()->language.'/tour/',
                'items' => $arraySubItems,
                'itemOptions' => array('class' => 'dropdown'),
                'linkOptions' => array('class' => 'dropdown-toggle', 'data-toggle' => 'dropdown')
            );
        }
        else if(count($model) > 0 && !$visibleChildLevels) {
            $array = array(
                'active' => Yii::app()->controller->id == 'tour' ? 1 : '',
                'label' => Yii::t('main-ui', 'Наши туры'),
                'url' => '/'.Yii::app()->language.'/tour/',
            );
        }
        else if(count($model) == 0) {
            $array = null;
        }
        return $array;

    }

    public function getAddMenuSouvenir($visibleChildLevels = true) {

        $criteria = new CDbCriteria();
        $criteria->with = array('Souvenir');
        $criteria->addCondition('t.visible = 1');
        $criteria->addCondition('Souvenir.visible = 1');
        $criteria->order = 't.sequence ASC';

        $model = SouvenirCategory::model()->findAll($criteria);

        $arraySubItem = array();
        $arraySubItems = array();
        if(count($model) > 0 && $visibleChildLevels) {
            foreach($model as $k => $item) {
                $arraySubItem['label'] = $item->name;
                $arraySubItem['url'] = '/'.Yii::app()->language.'/souvenir/?category%5B%5D='.$item->id_app_souvenir_category;
                $arraySubItems[$k] = $arraySubItem;
            }
            $array = array(
                'active' => Yii::app()->controller->id == 'souvenir' ? 1 : '',
                'label' =>  Yii::t('main-ui', 'Сувениры').' <b class="caret"></b>',
                'url' => '/'.Yii::app()->language.'/souvenir/',
                'items' => $arraySubItems,
                'itemOptions' => array('class' => 'dropdown'),
                'linkOptions' => array('class' => 'dropdown-toggle', 'data-toggle' => 'dropdown')
            );
        }
        else if(count($model) > 0 && !$visibleChildLevels) {
            $array = array(
                'active' => Yii::app()->controller->id == 'souvenir' ? 1 : '',
                'label' => Yii::t('main-ui', 'Сувениры'),
                'url' => '/'.Yii::app()->language.'/souvenir/',
            );
        }
        else if(count($model) == 0) {
            $array = null;
        }
        return $array;

    }

    public function getAddMenuInteresting($visibleChildLevels = true) {

        $criteria = new CDbCriteria();
        $criteria->with = array('Interesting');
        $criteria->addCondition('t.visible = 1');
        $criteria->addCondition('Interesting.visible = 1');
        $criteria->order = 't.sequence ASC';

        $model = InterestingCategory::model()->findAll($criteria);

        $arraySubItem = array();
        $arraySubItems = array();
        if(count($model) > 0 && $visibleChildLevels) {
            foreach($model as $k => $item) {
                $arraySubItem['label'] = $item->name;
                $arraySubItem['url'] = '/'.Yii::app()->language.'/interesting/?category%5B%5D='.$item->id_app_interesting_category;
                $arraySubItems[$k] = $arraySubItem;
            }
            $array = array(
                'active' => Yii::app()->controller->id == 'interesting' ? 1 : '',
                'label' =>  Yii::t('main-ui', 'Это интересно').' <b class="caret"></b>',
                'url' => '/'.Yii::app()->language.'/interesting/',
                'items' => $arraySubItems,
                'itemOptions' => array('class' => 'dropdown'),
                'linkOptions' => array('class' => 'dropdown-toggle', 'data-toggle' => 'dropdown')
            );
        }
        else if(count($model) > 0 && !$visibleChildLevels) {
            $array = array(
                'active' => Yii::app()->controller->id == 'interesting' ? 1 : '',
                'label' => Yii::t('main-ui', 'Это интересно'),
                'url' => '/'.Yii::app()->language.'/interesting/',
            );
        }
        else if(count($model) == 0) {
            $array = null;
        }
        return $array;

    }
}