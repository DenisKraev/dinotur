<?php

class NewToursWidget extends DaWidget {
	public function run() {

        $criteria = new CDbCriteria();
        $criteria->with = array('TourCategory');
        $criteria->addCondition('t.state = 1');
        $criteria->addCondition('t.in_main_page = 1');
        $criteria->addCondition('TourCategory.state = 1');
        $criteria->order = 't.sequence ASC';

        $model = Tour::model()->findAll($criteria);

        $this->render('newToursWidget', array(
            'model' => $model
        ));

	}
}
