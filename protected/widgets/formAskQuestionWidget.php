<?php

class formAskQuestionWidget extends DaWidget {
	public function run() {
        Yii::app()->user->setReturnUrl(Yii::app()->request->url);

        $model = BaseActiveRecord::newModel('FormFeedback');
        $this->render('formAskQuestionWidget', array('model' => $model));
    }
}
