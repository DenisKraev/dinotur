<?php

class PromoBlockWidget extends DaWidget {
	public function run() {

        $criteria=new CDbCriteria;
        $criteria->condition = 'state=:state';
        $criteria->params = array(':state'=>1);
        $criteria->order = 'sequence ASC';

        $model=PromoBlock::model()->findAll($criteria);

        $this->render('promoBlockWidget',array(
            'model'=>$model
        ));

	}
}
