<?php

class FormTourSelectionWidget extends DaWidget {
	public function run() {

        Yii::app()->user->setReturnUrl(Yii::app()->request->url);

        $criteria = new CDbCriteria();

        $criteria->with = array('TourCategory');
        $criteria->addCondition('t.state = 1');
        $criteria->addCondition('TourCategory.state = 1');
        $criteria->order = 't.sequence ASC';

        $modelTour = Tour::model()->findAll($criteria);

        $arrayTour = array();
        foreach($modelTour as $item) {
            $arrayTour[$item->name] = $item->name;
        }

        $model = BaseActiveRecord::newModel('TourSelection');

        $this->render('formTourSelectionWidget', array(
            'model' => $model,
            'arrayTour' => $arrayTour,
        ));
	}
}
