<?php

class FormSouvenirOrderWidget extends DaWidget {
	public function run() {

        Yii::app()->user->setReturnUrl(Yii::app()->request->url);
        $model = BaseActiveRecord::newModel('SouvenirOrder');

        $this->render('formSouvenirOrderWidget', array(
            'model' => $model,
        ));
	}
}