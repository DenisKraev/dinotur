<?php
    if (Yii::app()->user->hasFlash('form-souvenir-order-form-success')) {
        $this->widget('AlertWidget', array(
            'title' => Yii::t('main-ui', 'Заказать сувенир'),
            'message' => Yii::t('main-ui', Yii::app()->user->getFlash('form-souvenir-order-form-success')),
        ));
    }
    if (Yii::app()->user->hasFlash('form-souvenir-order-form-error')) {
        $this->widget('AlertWidget', array(
            'title' => Yii::t('main-ui', 'Заказать тур'),
            'message' => Yii::app()->user->getFlash('form-souvenir-order-form-error'),
            'type' => 'error'
        ));
    }
?>

<div id="box-form-souvenir-order" class="box-form-souvenir-order box-popup">

    <div class="header">
        <h4 class="title"><?php echo Yii::t('main-ui', 'Заказать сувенир'); ?></h4>
    </div>

    <div class="content-popup">

        <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id'=>'form-tour-souvenir-form',
                'enableAjaxValidation' => true,
                'enableClientValidation' => false,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                    'validateOnChange' => false,
                ),
                'action' => '/souvenir/ordersouvenir/',
            ));
        ?>

        <?php echo $form->hiddenField($model,'id_app_souvenir',array('class'=> 'style-form-text form-souvenir-id-souvenir')); ?>
        <?php echo $form->hiddenField($model,'name_souvenir',array('class'=> 'style-form-text form-souvenir-name-souvenir')); ?>
        <?php echo $form->hiddenField($model,'price',array('class'=> 'style-form-text form-souvenir-price')); ?>
        <?php echo $form->hiddenField($model,'parameters',array('class'=> 'style-form-textarea form-souvenir-parameters')); ?>

        <div class="row souvenir-data">
            <div class="img-souvenir col-md-5">
                <img src="">
            </div>
            <div class="col-md-7">
                <div class="name-title"><?php echo Yii::t('main-ui', 'Название'); ?>: <span></span></div>
                <div class="price-title"><?php echo Yii::t('main-ui', 'Цена'); ?>: <span></span> <?php echo Yii::t('main-ui', 'руб'); ?></div>
            </div>
        </div>

        <div class="form-row form-row-wishes">
            <?php echo $form->label($model,'name_sender', array('label' => Yii::t('main-ui', 'ФИО'), 'class'=> 'style-form-label')); ?>
            <?php echo $form->textField($model,'name_sender',array('class'=> 'style-form-text')); ?>
            <?php echo $form->error($model,'name_sender'); ?>
        </div>
        <div class="form-row form-row-wishes">
            <?php echo $form->label($model,'address', array('class'=> 'style-form-label')); ?>
            <?php echo $form->textarea($model,'address',array('class'=> 'style-form-text')); ?>
            <?php echo $form->error($model,'address'); ?>
        </div>
        <div class="form-row form-row-wishes">
            <?php echo $form->label($model,'email', array('class'=> 'style-form-label')); ?>
            <?php echo $form->textField($model,'email',array('class'=> 'style-form-text')); ?>
            <?php echo $form->error($model,'email'); ?>
        </div>
        <div class="form-row form-row-wishes">
            <?php echo $form->label($model,'phone', array('class'=> 'style-form-label')); ?>
            <?php echo $form->textField($model,'phone',array('class'=> 'style-form-text')); ?>
            <?php echo $form->error($model,'phone'); ?>
        </div>

        <div class="form-actions">
            <?php echo CHtml::submitButton(Yii::t('main-ui', 'Заказать'), array('class' => 'send-message style-btn')); ?>
            <?php echo CHtml::Button(Yii::t('main-ui', 'Отменить'), array('class' => 'popup-cancel style-btn style-btn-cancel')); ?>
        </div>

        <?php $this->endWidget(); ?>

    </div>
</div>