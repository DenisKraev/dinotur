<?php
    if (Yii::app()->user->hasFlash('form-tour-order-form-success')) {
        $this->widget('AlertWidget', array(
            'title' => Yii::t('main-ui', 'Заказать тур'),
            'message' => Yii::t('main-ui', Yii::app()->user->getFlash('form-tour-order-form-success')),
        ));
    }
    if (Yii::app()->user->hasFlash('form-tour-order-form-error')) {
        $this->widget('AlertWidget', array(
            'title' => Yii::t('main-ui', 'Заказать тур'),
            'message' => Yii::app()->user->getFlash('form-tour-order-form-error'),
            'type' => 'error'
        ));
    }
?>

<div id="box-form-tour-order" class="box-form-tour-order box-popup">

    <div class="header">
        <h4 class="title"><?php echo Yii::t('main-ui', 'Заказать тур'); ?></h4>
    </div>

    <div class="content-popup">

        <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id'=>'form-tour-order-form',
                'enableAjaxValidation' => true,
                'enableClientValidation' => false,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                    'validateOnChange' => false,
                ),
                'action' => '/tour/ordertour/',
            ));
        ?>

        <div class="form-row">
            <div class="select-tour-title style-form-label"><?php echo Yii::t('main-ui', 'Выберите один или несколько туров, в которых вы хотели бы побывать'); ?>:</div>
            <?php
                echo CHtml::checkBoxList(
                    'TourOrder[selected_tours]',
                    (isset($selectedTour)) ? $selectedTour : '',
                    $arrayTour,
                    array(
                        'template' => '{beginLabel}{input}<span>{labelTitle}</span>{endLabel}',
                        'class' => 'tourSelector style-form-checkbox',
                        'labelOptions' => array('class' => 'style-form-label cf'),
                        'separator' => '',
                        'container' => 'div'
                    )
                );
                echo $form->error($model,'selected_tours');
            ?>
        </div>

        <div class="form-row form-row-wishes">
            <?php echo $form->label($model,'wishes', array('label' => Yii::t('main-ui', 'Дополнительные пожелания, что вы ещё хотели бы увидеть').':', 'class'=> 'style-form-label')); ?>
            <?php echo $form->textArea($model,'wishes',array('class'=> 'style-form-textarea', 'size'=>60,'maxlength'=>255)); ?>
            <?php echo $form->error($model,'wishes'); ?>
        </div>

        <table class="order-add-options">
            <tr>
                <td class="td-label"><?php echo $form->label($model,'adults', array('class'=> 'style-form-label')); ?></td>
                <td>
                    <?php echo CHtml::dropDownList('TourOrder[adults]', '', $model->getCountParticipants(10), array('empty' => ' ', 'class'=> 'style-form-select'));?>
                    <?php echo $form->error($model,'adults'); ?>
                </td>
            </tr>
            <tr>
                <td class="td-label"><?php echo $form->label($model,'young_children', array('class'=> 'style-form-label')); ?></td>
                <td>
                    <?php echo CHtml::dropDownList('TourOrder[young_children]', '', $model->getCountParticipants(20), array('empty' => ' ', 'class'=> 'style-form-select'));?>
                    <?php echo $form->error($model,'young_children'); ?>
                </td>
            </tr>
            <tr>
                <td class="td-label"><?php echo $form->label($model,'older_children', array('class'=> 'style-form-label')); ?></td>
                <td>
                    <?php echo CHtml::dropDownList('TourOrder[older_children]', '', $model->getCountParticipants(20), array('empty' => ' ', 'class'=> 'style-form-select'));?>
                    <?php echo $form->error($model,'older_children'); ?>
                </td>
            </tr>
            <tr class="contacts-section-start">
                <td class="td-label"><?php echo $form->label($model,'name', array('class'=> 'style-form-label')); ?></td>
                <td>
                    <?php echo $form->textField($model,'name',array('class'=> 'style-form-text', 'size'=>60,'maxlength'=>255)); ?>
                    <?php echo $form->error($model,'name'); ?>
                </td>
            </tr>
            <tr>
                <td class="td-label"><?php echo $form->label($model,'email', array('class'=> 'style-form-label')); ?></td>
                <td>
                    <?php echo $form->textField($model,'email',array('class'=> 'style-form-text', 'size'=>60,'maxlength'=>255)); ?>
                    <?php echo $form->error($model,'email'); ?>
                </td>
            </tr>
            <tr>
                <td class="td-label"><?php echo $form->label($model,'phone', array('class'=> 'style-form-label')); ?></td>
                <td>
                    <?php echo $form->textField($model,'phone',array('class'=> 'style-form-text', 'size'=>60,'maxlength'=>255)); ?>
                    <?php echo $form->error($model,'phone'); ?>
                </td>
            </tr>
        </table>

        <div class="form-actions">
            <?php echo CHtml::submitButton(Yii::t('main-ui', 'Заказать'), array('class' => 'send-message style-btn')); ?>
            <?php echo CHtml::Button(Yii::t('main-ui', 'Отменить'), array('class' => 'popup-cancel style-btn style-btn-cancel')); ?>
        </div>

        <?php $this->endWidget(); ?>

    </div>
</div>