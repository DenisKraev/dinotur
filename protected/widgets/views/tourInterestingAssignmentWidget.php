<?php if ($model) { ?>
    <div class="tour-interesting-assignmen-widget">
        <div class="title"><?php echo Yii::t('main-ui', 'Достопримечательность встречается в следующих турах'); ?>:</div>
        <div class="box-list-items">
            <?php foreach($model as $data) { ?>
                <div class="box-single-item ib">
                    <a href="<?php echo $data->url; ?>" class="box-img">
                        <?php if($data->new_item) { ?>
                            <div class="ribbon_<?php echo Yii::app()->language; ?>"></div>
                        <?php } ?>
                        <img src="<?php echo $data->getImagePreview('_main_image')->getUrlPath() ?>" alt="<?php echo AppHelper::cutQuotes($data->name); ?>"/>
                    </a>
                    <h3 class="name"><a href="<?php echo $data->url; ?>" title="<?php echo $data->name; ?>"><?php echo HText::smartCrop($data->name, 45); ?></a></h3>
                    <div class="more">
                        <a class="brown-btn" href="<?php echo $data->url; ?>"><?php echo Yii::t('main-ui', 'Подробнее'); ?></a>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
<?php } ?>