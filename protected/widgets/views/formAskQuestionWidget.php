<?php
    if (Yii::app()->user->hasFlash('form-feedback-success')) {
        $this->widget('AlertWidget', array(
            'title' => Yii::t('main-ui', 'Задать вопрос'),
            'message' => Yii::app()->user->getFlash('form-feedback-success'),
    ));
}
?>
<div class="box-form-ask-question">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id'=>'form-ask-question-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => false,
        ),
        'action' => '/contact/askquestion/',
    ));
    ?>

    <div class="row">
        <div class="col-md-6">
            <div class="form-row">
                <?php echo $form->textField($model,'name',array('class'=> 'style-form-text', 'size'=>60,'maxlength'=>255, 'placeholder'=>Yii::t('main-ui', 'Ваше имя'))); ?>
                <?php echo $form->error($model,'name'); ?>
            </div>

            <div class="form-row">
                <?php echo $form->textField($model,'phone',array('class'=> 'style-form-text', 'size'=>60,'maxlength'=>255, 'placeholder'=>Yii::t('main-ui', 'Ваш телефон'))); ?>
                <?php echo $form->error($model,'phone'); ?>
            </div>

            <div class="form-row">
                <?php echo $form->emailField($model,'email',array('class'=> 'style-form-text', 'size'=>60,'maxlength'=>255, 'placeholder'=>Yii::t('main-ui', 'Ваш e-mail'))); ?>
                <?php echo $form->error($model,'email'); ?>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-row">
                <?php echo $form->textArea($model,'text',array('class'=> 'style-form-textarea', 'rows'=>6, 'cols'=>50, 'placeholder'=>Yii::t('main-ui', 'Текст сообщения'))); ?>
                <?php echo $form->error($model,'text'); ?>
            </div>
        </div>
    </div>

    <div class="form-actions">
        <?php echo CHtml::submitButton(Yii::t('main-ui', 'Написать'), array('class' => 'style-btn style-btn-big')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div>