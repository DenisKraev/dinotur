<?php
if (Yii::app()->user->hasFlash('form-review-form-success')) {
    $this->widget('AlertWidget', array(
        'title' => Yii::t('main-ui', 'Написать отзыв'),
        'message' => Yii::t('main-ui', Yii::app()->user->getFlash('form-review-form-success')),
    ));
}
if (Yii::app()->user->hasFlash('form-review-form-error')) {
    $this->widget('AlertWidget', array(
        'title' => Yii::t('main-ui', 'Написать отзыв'),
        'message' => Yii::app()->user->getFlash('form-review-form-error'),
        'type' => 'error'
    ));
}
?>
<div class="box-review">
    <div class="top cf">
        <h2 class="tab-title"><?php echo Yii::t('main-ui', 'Отзывы') ?></h2>
    </div>
    <div class="box-get-form-review-btn">
        <i class="arrow ib"></i><span class="get-form-review-btn js-get-form-review-btn"><?php echo Yii::t('main-ui', 'Написать отзыв') ?></span>
    </div>
    <div class="box-form-review">
        <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id'=>'form-review-form',
                'enableAjaxValidation' => true,
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                    'validateOnChange' => false,
                ),
                'action' => '/tour/writereview/',
            ));
        ?>

        <?php echo $form->hiddenField($model,'id_app_tour',array('value'=>$selectedId)); ?>

        <div class="form-row">
            <?php echo $form->label($model,'name_sender', array('class'=> 'style-form-label name-sender-label')); ?>
            <?php echo $form->textField($model,'name_sender',array('class'=> 'style-form-text name-sender-text', 'size'=>60,'maxlength'=>255)); ?>
            <?php echo $form->error($model,'name_sender'); ?>
        </div>

        <div class="form-row">
            <?php echo $form->label($model,'text', array('class'=> 'style-form-label')); ?>
            <?php echo $form->textarea($model,'text',array('class'=> 'style-form-textarea', 'size'=>60,'maxlength'=>255)); ?>
            <?php echo $form->error($model,'text'); ?>
        </div>

        <div class="form-actions">
            <?php echo CHtml::submitButton(Yii::t('main-ui', 'Оставить отзыв'), array('class' => 'brown-btn')); ?>
        </div>

        <?php $this->endWidget(); ?>
    </div>
    <div class="review-list">
        <?php foreach ($modelReviewList as $modelItem) { ?>
            <div class="review-item">
                <div class="top-review-item">
                    <span class="name-sender"><?php echo $modelItem->name_sender; ?></span>
                    <span class="date"><?php echo Yii::app()->dateFormatter->format('dd.MM.yyyy', $modelItem->datetime); ?></span>
                </div>
                <div class="text"><?php echo $modelItem->text; ?></div>
            </div>
        <?php } ?>
    </div>
</div>