<?php if (count($model) > 0) { ?>
<div class="box-new-items">
    <div class="container">
        <h3 class="title-main"><?php echo Yii::t('main-ui', 'Новинки'); ?>:</h3>
        <div class="new-items-list box-list-items">
            <?php foreach ($model as $modelItem) { ?>
                <div class="new-item box-single-item ib">
                    <a href="<?php echo $modelItem->url; ?>" class="box-img">
                        <?php if($modelItem->new_item) { ?>
                            <div class="ribbon_<?php echo Yii::app()->language; ?>"></div>
                        <?php } ?>
                        <img src="<?php echo $modelItem->getImagePreview('_main_image')->getUrlPath() ?>" alt="<?php echo AppHelper::cutQuotes($modelItem->name); ?>" />
                    </a>
                    <h3 class="title-new-item"><a href="<?php echo $modelItem->url; ?>" title="<?php echo $modelItem->name; ?>"><?php echo HText::smartCrop($modelItem->name, 45); ?></a></h3>
                    <div class="duration-tour"><?php echo Yii::t('main-ui', '{n} день|{n} дня|{n} дней|{n} дня', $modelItem->duration); ?></div>
                    <div class="description"><?php echo HText::smartCrop($modelItem->description, 110); ?></div>
                </div>
            <?php } ?>
        </div>
        <div class="box-respond-btn">
            <h4 class="title-main"><?php echo Yii::t('main-ui', 'Подписка на новинки и новости'); ?></h4>
            <a href="#popup-respond" class="style-btn style-btn-big js-main-popup"><?php echo Yii::t('main-ui', 'Подписаться'); ?></a>
        </div>
    </div>
</div>
<?php } ?>