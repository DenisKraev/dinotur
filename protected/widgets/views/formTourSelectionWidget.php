<?php
    if (Yii::app()->user->hasFlash('form-tour-selection-form-success')) {
        $this->widget('AlertWidget', array(
            'title' => Yii::t('main-ui', 'Подобрать тур'),
            'message' => Yii::t('main-ui', Yii::app()->user->getFlash('form-tour-selection-form-success')),
        ));
    }
    if (Yii::app()->user->hasFlash('form-tour-selection-form-error')) {
        $this->widget('AlertWidget', array(
            'title' => Yii::t('main-ui', 'Подобрать тур'),
            'message' => Yii::app()->user->getFlash('form-tour-selection-form-error'),
            'type' => 'error'
        ));
    }
?>

<div id="box-form-tour-selection" class="box-form-tour-selection box-popup">

    <div class="header">
        <h4 class="title"><?php echo Yii::t('main-ui', 'Подобрать тур'); ?></h4>
    </div>

    <div class="content-popup">

        <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id'=>'form-tour-selection-form',
                'enableAjaxValidation' => true,
                'enableClientValidation' => false,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                    'validateOnChange' => false,
                ),
                'action' => '/tour/selectiontour/',
            ));
        ?>

        <div class="form-row">
            <?php echo $form->label($model,'name', array('class'=> 'style-form-label')); ?>
            <?php echo $form->textField($model,'name',array('class'=> 'style-form-text')); ?>
            <?php echo $form->error($model,'name'); ?>
        </div>

        <div class="form-row">
            <?php echo $form->label($model,'planned_date', array('class'=> 'style-form-label')); ?>
            <?php echo $form->textField($model,'planned_date',array('class'=> 'style-form-text style-form-datepicker')); ?>
            <?php echo $form->error($model,'planned_date'); ?>
        </div>

        <div class="form-row">
            <?php echo $form->label($model,'days_stay', array('class'=> 'style-form-label')); ?>
            <?php echo $form->textField($model,'days_stay',array('class'=> 'style-form-text')); ?>
            <?php echo $form->error($model,'days_stay'); ?>
        </div>

        <div class="form-row">
            <?php echo $form->label($model,'town_out', array('class'=> 'style-form-label')); ?>
            <?php echo $form->textField($model,'town_out',array('class'=> 'style-form-text')); ?>
            <?php echo $form->error($model,'town_out'); ?>
        </div>

        <div class="form-row">
            <?php echo $form->label($model,'plan_get', array('class'=> 'style-form-label')); ?>
            <?php echo $form->textarea($model,'plan_get',array('class'=> 'style-form-textarea')); ?>
            <?php echo $form->error($model,'plan_get'); ?>
        </div>

        <div class="form-row">
            <?php echo $form->label($model,'full_years', array('class'=> 'style-form-label')); ?>
            <?php echo $form->textField($model,'full_years',array('class'=> 'style-form-text')); ?>
            <?php echo $form->error($model,'full_years'); ?>
        </div>

        <div class="form-row">
            <?php echo $form->label($model,'composition_group', array('class'=> 'style-form-label')); ?>
            <?php echo $form->textarea($model,'composition_group',array('class'=> 'style-form-textarea')); ?>
            <?php echo $form->error($model,'composition_group'); ?>
        </div>

        <div class="form-row">
            <?php echo $form->label($model,'young_children', array('class'=> 'style-form-label')); ?>
            <?php echo $form->textField($model,'young_children',array('class'=> 'style-form-text')); ?>
            <?php echo $form->error($model,'young_children'); ?>
        </div>

        <div class="form-row">
            <?php echo $form->checkbox($model,'pets_with',array('class'=> 'style-form-checkbox')); ?>
            <?php echo $form->label($model,'pets_with', array('class'=> 'style-form-label')); ?>
            <?php echo $form->error($model,'pets_with'); ?>
        </div>

        <div class="form-row">
            <?php echo $form->checkbox($model,'insurance_with',array('class'=> 'style-form-checkbox')); ?>
            <?php echo $form->label($model,'insurance_with', array('class'=> 'style-form-label')); ?>
            <?php echo $form->error($model,'insurance_with'); ?>
        </div>

        <div class="form-row tourism-prefer">
            <?php echo $form->label($model,'tourism_prefer', array('class'=> 'style-form-label')); ?>
            <?php
            echo CHtml::checkBoxList(
                'TourSelection[tourism_prefer]',
                '',
                $model->getTourismPrefer(),
                array(
                    'template' => '{beginLabel}{input}<span>{labelTitle}</span>{endLabel}',
                    'class' => 'tourSelector style-form-checkbox',
                    'labelOptions' => array('class' => 'style-form-label cf'),
                    'separator' => '',
                    'container' => 'div'
                )
            );
            echo $form->error($model,'tourism_prefer');
            ?>
        </div>

        <div class="form-row">
            <?php echo $form->label($model,'our_tours', array('class'=> 'style-form-label')); ?>
            <?php
                echo CHtml::checkBoxList(
                    'TourSelection[our_tours]',
                    '',
                    $arrayTour,
                    array(
                        'template' => '{beginLabel}{input}<span>{labelTitle}</span>{endLabel}',
                        'class' => 'tourSelector style-form-checkbox',
                        'labelOptions' => array('class' => 'style-form-label cf'),
                        'separator' => '',
                        'container' => 'div'
                    )
                );
                echo $form->error($model,'our_tours');
            ?>
        </div>

        <div class="form-row">
            <?php echo $form->label($model,'wishes', array('class'=> 'style-form-label')); ?>
            <?php echo $form->textArea($model,'wishes',array('class'=> 'style-form-textarea', 'size'=>60,'maxlength'=>255)); ?>
            <?php echo $form->error($model,'wishes'); ?>
        </div>

        <div class="form-row">
            <?php echo $form->label($model,'phone', array('class'=> 'style-form-label')); ?>
            <?php echo $form->textField($model,'phone',array('class'=> 'style-form-text')); ?>
            <?php echo $form->error($model,'phone'); ?>
        </div>

        <div class="form-row">
            <?php echo $form->label($model,'additional_phone', array('class'=> 'style-form-label')); ?>
            <?php echo $form->textField($model,'additional_phone',array('class'=> 'style-form-text')); ?>
            <?php echo $form->error($model,'additional_phone'); ?>
        </div>

        <div class="form-row">
            <?php echo $form->label($model,'email', array('class'=> 'style-form-label')); ?>
            <?php echo $form->textField($model,'email',array('class'=> 'style-form-text')); ?>
            <?php echo $form->error($model,'email'); ?>
        </div>

        <div class="form-actions">
            <?php echo CHtml::submitButton(Yii::t('main-ui', 'Подобрать'), array('class' => 'send-message style-btn')); ?>
            <?php echo CHtml::Button(Yii::t('main-ui', 'Отменить'), array('class' => 'popup-cancel style-btn style-btn-cancel')); ?>
        </div>

        <?php $this->endWidget(); ?>

    </div>
</div>