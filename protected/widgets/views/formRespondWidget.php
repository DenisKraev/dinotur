<div id="popup-respond" class="popup-respond box-popup">
    <!-- SmartResponder.ru subscribe form code (begin) -->
<!--    <link rel="stylesheet" href="https://imgs.smartresponder.ru/e1bbeb24091b44f1f4048bbc87edacd11278fd23/">-->
<!--    <script type="text/javascript" src="https://imgs.smartresponder.ru/52568378bec6f68117c48f2f786db466014ee5a0/"></script>-->
<!--    <script type="text/javascript">-->
<!--        _sr(function() {-->
<!--            _sr('form[name="SR_form_289011_26"]').find('div#sr-preload_').prop('id', 'sr-preload_289011_26');-->
<!--            _sr('#sr-preload_289011_26').css({'width':parseInt(_sr('form[name="SR_form_289011_26"]').width() + 'px'), 'height':parseInt(_sr('form[name="SR_form_289011_26"]').height()) + 'px', 'line-height':parseInt(_sr('form[name="SR_form_289011_26"]').height()) + 'px'}).show();-->
<!--            if(_sr('form[name="SR_form_289011_26"]').find('input[name="script_url_289011_26"]').length) {-->
<!--                _sr.ajax({-->
<!--                    url: _sr('input[name="script_url_289011_26"]').val() + '/' + (typeof document.charset !== 'undefined' ? document.charset : document.characterSet),-->
<!--                    dataType: "script",-->
<!--                    success: function() {-->
<!--                        _sr('#sr-preload_289011_26').hide();-->
<!--                    }-->
<!--                });-->
<!--            }-->
<!--        });-->
<!--    </script>-->
    <div id="outer_alignment" align="left">
        <form class="sr-box" method="post" action="https://smartresponder.ru/subscribe.html" target="_blank" name="SR_form_289011_26">
            <input type="text" name="field_name" class="sr-name" style="display: none">
            <ul class="sr-box-list">
                <li class="sr-289011_26" >
                    <div class="header">
                        <h4 class="title"><?php echo Yii::t('main-ui', 'Подписка на рассылку'); ?></h4>
                    </div>
                    <input type="hidden" name="element_header" value="" style="font-family: Arial; color: rgb(0, 0, 0); font-size: 12px; font-style: normal; font-weight: normal; border: none; box-shadow: none; background-color: rgb(255, 255, 255);">
                </li>
                <div class="content-popup">
                    <li class="sr-289011_26 form-row">
                        <input type="text" name="field_name_first" class="style-form-text" placeholder="<?php echo Yii::t('main-ui', 'Ваше имя'); ?>">
                    </li>
                    <li class="sr-289011_26 form-row">
                        <input type="text" name="field_email" class="sr-required style-form-text" placeholder="<?php echo Yii::t('main-ui', 'Ваш email'); ?>">
                    </li>
                    <li class="sr-289011_26">
                        <div class="form-actions">
                            <input type="submit" name="subscribe" value="<?php echo Yii::t('main-ui', 'Подписаться'); ?>" class="style-btn">
                        </div>
                    </li>
                </div>
            </ul>
            <input type="hidden" name="uid" value="614164">
            <input type="hidden" name="did[]" value="741148">
            <input type="hidden" name="tid" value="0">
            <input type="hidden" name="lang" value="ru">
            <input name="script_url_289011_26" type="hidden" value="https://imgs.smartresponder.ru/on/0c4d3683566546252a6b5bdf26191bc2910c0cbb/289011_26">
        </form>
    </div>
    <!-- SmartResponder.ru subscribe form code (end) -->
</div>