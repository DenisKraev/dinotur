<?php
    if (Yii::app()->user->hasFlash('form-callback-success')) {
        $this->widget('AlertWidget', array(
            'title' => Yii::t('main-ui', 'Форма обратного звонка'),
            'message' => Yii::app()->user->getFlash('form-callback-success'),
        ));
    }
?>

<div id="box-form-callback" class="box-form-callback box-popup">

    <div class="header">
        <h4 class="title"><?php echo Yii::t('main-ui', 'Форма обратного звонка'); ?></h4>
    </div>

    <div class="content-popup">

        <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id'=>'form-callback-form',
                'enableAjaxValidation' => false,
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                    'validateOnChange' => false,
                ),
                'action' => '/contact/sendcallback/',
            ));
        ?>

        <div class="cf">
            <div class="form-row">
                <?php echo $form->label($model,'name', array('class'=> 'style-form-label')); ?>
                <?php echo $form->textField($model,'name',array('class'=> 'style-form-text', 'size'=>60,'maxlength'=>255)); ?>
                <?php echo $form->error($model,'name'); ?>
            </div>

            <div class="form-row">
                <?php echo $form->label($model,'phone', array('class'=> 'style-form-label')); ?>
                <?php echo $form->textField($model,'phone',array('class'=> 'style-form-text', 'size'=>60,'maxlength'=>255)); ?>
                <?php echo $form->error($model,'phone'); ?>
            </div>
        </div>

        <div class="form-actions">
            <?php echo CHtml::submitButton(Yii::t('main-ui', 'Заказать звонок'), array('class' => 'send-message style-btn')); ?>
        </div>

        <?php $this->endWidget(); ?>

    </div>
</div>