<?php if(!empty($model)) { ?>

    <div class="box-promo-items">
        <div class="cf">
            <?php foreach($model as $item) { ?>
                <div class="col-md-4">
                    <?php if(!empty($item->url)) { ?>
                        <a href="<?php echo $item->url ?>" class="box-promo-item box-interesting">
                    <?php } else { echo '<div class="box-promo-item box-interesting">'; }?>
                        <?php if(!empty($item->bgImage)) { ?>
                            <img class="promo-bg" src="<?php echo $item->getImagePreview('_bg')->getUrlPath(); ?>" />
                        <?php } ?>
                        <div class="promo-item-content">
                            <?php if(!empty($item->title)) { ?>
                                <h3 class="title"><?php echo $item->title ?></h3>
                            <?php } ?>
                            <?php if(!empty($item->text)) { ?>
                                <div class="text"><?php echo HText::smartCrop($item->text, 55) ?></div>
                            <?php } ?>
                            <?php if(!empty($item->url)) { ?>
                                <span class="more"><?php echo Yii::t('main-ui', 'Подробнее'); ?></span>
                            <?php } ?>
                        </div>
                    <?php if(!empty($item->url)) { ?>
                        </a>
                    <?php } else { echo '</div>'; }?>
                </div>
            <?php } ?>
        </div>
    </div>

<?php } ?>