<?php
    if (Yii::app()->user->hasFlash('form-feedback-success')) {
        $this->widget('AlertWidget', array(
            'title' => Yii::t('main-ui', 'Написать нам'),
            'message' => Yii::app()->user->getFlash('form-feedback-success'),
    ));
}
?>

<div id="box-form-contact-us" class="box-form-contact-us box-popup">

    <div class="header">
        <h4 class="title"><?php echo Yii::t('main-ui', 'Написать нам'); ?></h4>
    </div>
    <div class="content-popup">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id'=>'form-contact-us-form',
            'enableAjaxValidation' => false,
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
                'validateOnChange' => false,
            ),
            'action' => '/contact/sendcontactus/',
        ));
        ?>

        <div class="cf">
            <div class="left-part">
                <div class="form-row">
                    <?php echo $form->label($model,'name', array('class'=> 'style-form-label')); ?>
                    <?php echo $form->textField($model,'name',array('class'=> 'style-form-text', 'size'=>60,'maxlength'=>255)); ?>
                    <?php echo $form->error($model,'name'); ?>
                </div>

                <div class="form-row">
                    <?php echo $form->label($model,'phone', array('class'=> 'style-form-label')); ?>
                    <?php echo $form->textField($model,'phone',array('class'=> 'style-form-text', 'size'=>60,'maxlength'=>255)); ?>
                    <?php echo $form->error($model,'phone'); ?>
                </div>

                <div class="form-row">
                    <?php echo $form->label($model,'email', array('class'=> 'style-form-label')); ?>
                    <?php echo $form->emailField($model,'email',array('class'=> 'style-form-text', 'size'=>60,'maxlength'=>255)); ?>
                    <?php echo $form->error($model,'email'); ?>
                </div>
            </div>
            <div class="right-part">
                <div class="form-row form-row-textarea">
                    <?php echo $form->label($model,'text', array('class'=> 'style-form-label')); ?>
                    <?php echo $form->textArea($model,'text',array('class'=> 'style-form-textarea', 'rows'=>6, 'cols'=>50)); ?>
                    <?php echo $form->error($model,'text'); ?>
                </div>
            </div>
        </div>

        <div class="form-actions">
            <?php echo CHtml::submitButton(Yii::t('main-ui', 'Написать'), array('class' => 'style-btn')); ?>
        </div>

        <?php $this->endWidget(); ?>
    </div>

</div>