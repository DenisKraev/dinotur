<?php

class ReviewWidget extends DaWidget {
    public $selectedId;

	public function run() {

        Yii::app()->user->setReturnUrl(Yii::app()->request->url);

        $criteria = new CDbCriteria();

        $criteria->with = array('Tour');
        $criteria->addCondition('t.accepted = 1');
        $criteria->addInCondition('Tour.alias', array($_GET["id"]));
        $criteria->order = 't.datetime DESC';

        $modelReviewList = Review::model()->findAll($criteria);

        $model = BaseActiveRecord::newModel('Review');

        $this->render('reviewWidget', array(
            'model' => $model,
            'modelReviewList' => $modelReviewList,
            'selectedId' => $this->selectedId,
        ));

	}
}
