<?php

class TourInterestingAssignmentWidget extends DaWidget {

    public $selectedId;

	public function run() {

        $criteria = new CDbCriteria();
        $criteria->with = array('Tour');
        $criteria->addCondition('Tour.state = 1');
        $criteria->order = 'Tour.sequence ASC';
        $criteria->addInCondition('t.id_app_interesting', array($this->selectedId));

        $model = Interesting::model()->findAll($criteria);
        $model = $model[0]->Tour;

	    $this->render('tourInterestingAssignmentWidget', array('model' => $model));
	}
}