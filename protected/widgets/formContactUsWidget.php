<?php

class formContactUsWidget extends DaWidget {

    public function run() {
        Yii::app()->user->setReturnUrl(Yii::app()->request->url);

        $model = BaseActiveRecord::newModel('FormContactUs');
        $this->render('formContactUsWidget', array('model' => $model));
    }

}