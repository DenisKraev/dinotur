<?php

class formTourOrderWidget extends DaWidget {
	public function run() {
        Yii::app()->user->setReturnUrl(Yii::app()->request->url);

        $criteria = new CDbCriteria();

        $criteria->with = array('TourCategory');
        $criteria->addCondition('t.state = 1');
        $criteria->addCondition('TourCategory.state = 1');
        $criteria->order = 't.sequence ASC';

        $modelTour = Tour::model()->findAll($criteria);

        $arrayTour = array();
        foreach($modelTour as $item) {
            if ($item->alias == $_GET['id']){$selectedTour = $item->name;}
            $arrayTour[$item->name] = $item->name;
        }

        $model = BaseActiveRecord::newModel('TourOrder');

        $this->render('formTourOrderWidget', array(
            'model' => $model,
            'arrayTour' => $arrayTour,
            'selectedTour' => $selectedTour
        ));
	}
}