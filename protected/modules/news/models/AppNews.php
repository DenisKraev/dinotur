<?php

class AppNews extends News {

    public function behaviors() {
        $behaviors = array(
            'ImagePreviewBehavior' => array(
                'class' => 'ImagePreviewBehavior',
                'imageProperty' => 'image',
                'formats' => array(
                    '_list' => array(
                        'width' => '299',
                        'height' => '218',
                        'crop' => 'center',
                    ),
                    '_list_small' => array(
                        'width' => '79',
                        'height' => '58',
                        'crop' => 'center',
                    ),
                ),
            ),
        );
        return $behaviors;
    }

    private function getAttributeByLanguage($attr)  {
        $attr .= '_'.Yii::app()->language;
        return $attr;
    }

    public function getTitle()  {
        $attr = $this->getAttributeByLanguage('title');
        return $this->$attr;
    }

    public function getContent()  {
        $attr = $this->getAttributeByLanguage('content');
        return $this->$attr;
    }

    public function getShort()  {
        $attr = $this->getAttributeByLanguage('short');
        return $this->$attr;
    }

}